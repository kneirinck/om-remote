package net.rivercrew.domotics.omremote.adapters;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;

import net.rivercrew.domotics.omremote.IUnsubscribable;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.models.CombinedOutput;
import net.rivercrew.domotics.omremote.models.CombinedShutter;
import net.rivercrew.domotics.omremote.outputs.OutputStatus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class CombinedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Action1<Object>, IUnsubscribable {
	private final static int TYPE_OUTPUT = 0;
	private final static int TYPE_SHUTTER = 1;
	private final ArrayList<CombinedOutput> mOutputList;
	private final ArrayList<Subscription> mSubscriptions;
	private final OnOutputMenuItemClickListener mOutputMenuItemClickListener;
	private final ArrayList<CombinedShutter> mShutterList;
	private final OnShutterMenuItemClickListener mShutterMenuItemClickListener;

	public CombinedAdapter(OnOutputMenuItemClickListener outputMenuItemClickListener, OnShutterMenuItemClickListener shutterMenuItemClickListener) {
		this.mOutputMenuItemClickListener = outputMenuItemClickListener;
		this.mOutputList = CombinedOutput.getAllFromDatabase();

		this.mSubscriptions = new ArrayList<>();
		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.OutputInfos.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));

		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.OutputStatus.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));


		this.mShutterMenuItemClickListener = shutterMenuItemClickListener;
		this.mShutterList = CombinedShutter.getAllFromDatabase();

		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.ShutterInfos.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));
	}

	@Override
	public int getItemViewType(int position) {
		if (position < mOutputList.size()) {
			return TYPE_OUTPUT;
		} else {
			return TYPE_SHUTTER;
		}
	}

	@Override
	public void unsubscribe() {
		for (Subscription subscription : this.mSubscriptions) {
			subscription.unsubscribe();
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == TYPE_OUTPUT) {
			return new OutputViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_output, parent, false),
				mOutputMenuItemClickListener);
		} else {
			return new ShutterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shutter, parent, false),
				mShutterMenuItemClickListener);
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		if (getItemViewType(position) == TYPE_OUTPUT) {
			((OutputViewHolder) holder).bind(this.mOutputList.get(position));
		} else {
			((ShutterViewHolder) holder).bind(this.mShutterList.get(position - mOutputList.size()));
		}
	}

	@Override
	public int getItemCount() {
		return this.mOutputList.size() + mShutterList.size();
	}

	@Override
	public void call(Object o) {
		if (o instanceof OpenMoticsClient.OutputInfos) {
			for (OpenMoticsClient.OutputInfo info : (OpenMoticsClient.OutputInfos) o) {
				int index = 0;
				for (CombinedOutput output : this.mOutputList) {
					if (output.updateStatus(info.id, info.status)) {
						this.notifyItemChanged(index);
					}
					index++;
				}
			}
			this.notifyDataSetChanged();
		} else if (o instanceof OpenMoticsClient.OutputStatus) {
			final OpenMoticsClient.OutputStatus status = (OpenMoticsClient.OutputStatus) o;

			int index = 0;
			for (CombinedOutput output : this.mOutputList) {
				if (output.updateStatus(status.id, status.status)) {
					this.notifyItemChanged(index);
				}
				index++;
			}
			boolean needsRefresh = false;
			for (CombinedShutter shutter : this.mShutterList) {
				if (shutter.getShutterIds().contains(status.id)) {
					needsRefresh = true;
				}
			}
			if (needsRefresh) {
				OpenMoticsClient.getInstance().refresh();
			}
		} else if (o instanceof OpenMoticsClient.ShutterInfos) {
			for (OpenMoticsClient.ShutterInfo info : (OpenMoticsClient.ShutterInfos) o) {
				int index = 0;
				for (CombinedShutter shutter : this.mShutterList) {
					if (shutter.updateStatus(info.id, info.status)) {
						this.notifyItemChanged(index + mOutputList.size());
					}
					index++;
				}
			}
			this.notifyDataSetChanged();
		}
	}

	public void add(CombinedOutput output) {
		this.mOutputList.add(output);
		this.notifyDataSetChanged();
	}

	public void delete(CombinedOutput combinedOutput) {
		if (!this.mOutputList.contains(combinedOutput)) {
			return;
		}
		final int position = this.mOutputList.indexOf(combinedOutput);
		this.mOutputList.remove(position);
		this.notifyItemRemoved(position);
	}

	public void update(CombinedOutput combinedOutput) {
		for (int i = 0; i < this.mOutputList.size(); ++i) {
			if (this.mOutputList.get(i).getCombinedId() == combinedOutput.getCombinedId()) {
				this.mOutputList.set(i, combinedOutput);
				this.notifyItemChanged(i);
				return;
			}
		}
	}

	public void add(CombinedShutter shutter) {
		this.mShutterList.add(shutter);
		this.notifyDataSetChanged();
	}

	public void delete(CombinedShutter combinedShutter) {
		if (!this.mShutterList.contains(combinedShutter)) {
			return;
		}
		final int position = this.mShutterList.indexOf(combinedShutter);
		this.mShutterList.remove(position);
		this.notifyItemRemoved(position + mOutputList.size());
	}

	public void update(CombinedShutter combinedShutter) {
		for (int i = 0; i < this.mShutterList.size(); ++i) {
			if (this.mShutterList.get(i).getCombinedId() == combinedShutter.getCombinedId()) {
				this.mShutterList.set(i, combinedShutter);
				this.notifyItemChanged(i + mOutputList.size());
				return;
			}
		}
	}

	protected static class OutputViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener, PopupMenu.OnMenuItemClickListener {
		private final OnOutputMenuItemClickListener mMenuItemClickListener;
		private CombinedOutput mOutput;
		@BindView(R.id.button)
		protected ActionProcessButton mButton;
		@BindView(R.id.text)
		protected TextView mText;
		@BindView(R.id.icon)
		protected ImageView mIcon;

		public OutputViewHolder(View view, OnOutputMenuItemClickListener menuItemClickListener) {
			super(view);

			this.mMenuItemClickListener = menuItemClickListener;

			ButterKnife.bind(this, view);
			view.setOnLongClickListener(this);
			this.mButton.setMode(ActionProcessButton.Mode.ENDLESS);
		}

		public void bind(CombinedOutput output) {
			this.mOutput = output;

			switch (output.getStatus()) {
				case ON:
					this.mButton.setProgress(100);
					break;
				case OFF:
					this.mButton.setProgress(-1);
					break;
				case UNKNOWN:
				default:
					this.mButton.setProgress(50);
			}

			this.mIcon.setImageResource(R.drawable.unknown);
			this.mText.setText(output.getName());

			this.mButton.setVisibility(View.VISIBLE);
		}

		@OnClick(R.id.button)
		public void onClick(View view) {
			if (this.mButton.getProgress() == -1) {
				for (Integer id : this.mOutput.getOutputIds()) {
					OpenMoticsClient.getInstance().setOutput(id, true);
				}
			} else if (this.mButton.getProgress() == 100) {
				for (Integer id : this.mOutput.getOutputIds()) {
					OpenMoticsClient.getInstance().setOutput(id, false);
				}
			}
			this.mButton.setProgress(50);
		}

		@Override
		public boolean onLongClick(View v) {
			final PopupMenu popup = new PopupMenu(v.getContext(), v);
			popup.setOnMenuItemClickListener(this);
			popup.inflate(R.menu.menu_combined_output);
			popup.show();
			return true;
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			return this.mMenuItemClickListener.onMenuItemClick(item, mOutput);
		}
	}

	protected static class ShutterViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, PopupMenu.OnMenuItemClickListener {
		private final OnShutterMenuItemClickListener mMenuItemClickListener;
		private CombinedShutter mShutter;
		@BindView(R.id.text)
		protected TextView mText;
		@BindView(R.id.icon)
		ImageView mIcon;
		@BindView(R.id.button_up)
		protected ActionProcessButton mButtonUp;
		@BindView(R.id.button_down)
		protected ActionProcessButton mButtonDown;

		public ShutterViewHolder(View view, OnShutterMenuItemClickListener menuItemClickListener) {
			super(view);

			this.mMenuItemClickListener = menuItemClickListener;

			ButterKnife.bind(this, view);
			view.setOnLongClickListener(this);
			this.mButtonUp.setMode(ActionProcessButton.Mode.ENDLESS);
			this.mButtonDown.setMode(ActionProcessButton.Mode.ENDLESS);
		}

		public void bind(CombinedShutter shutter) {
			this.mShutter = shutter;

			updateButton(this.mButtonUp, mShutter.getUpStatus());
			updateButton(this.mButtonDown, mShutter.getDownStatus());

			this.mIcon.setImageResource(R.drawable.shutter);
			this.mText.setText(shutter.getName());

			this.mButtonUp.setVisibility(View.VISIBLE);
			this.mButtonDown.setVisibility(View.VISIBLE);
		}

		void updateButton(ActionProcessButton button, OutputStatus status) {
			switch (status) {
				case ON:
					button.setProgress(100);
					break;
				case OFF:
					button.setProgress(-1);
					break;
				case UNKNOWN:
				default:
					button.setProgress(50);
			}
		}

		@OnClick(R.id.button_up)
		void onClickUp(@SuppressWarnings("unused") View view) {
			if (this.mButtonUp.getProgress() == -1) {
				for (Integer id : this.mShutter.getShutterIds()) {
					if (this.mShutter.getUpStatus() == OutputStatus.OFF) {
						OpenMoticsClient.getInstance().setShutter(id, true);
					} else if (this.mShutter.getUpStatus() == OutputStatus.ON) {
						OpenMoticsClient.getInstance().stopShutter(id);
					}
				}
			} else if (this.mButtonUp.getProgress() == 100) {
				for (Integer id : this.mShutter.getShutterIds()) {
					OpenMoticsClient.getInstance().stopShutter(id);
				}
			}
			this.mButtonUp.setProgress(50);
		}

		@OnClick(R.id.button_down)
		void onClickDown(@SuppressWarnings("unused") View view) {
			if (this.mButtonDown.getProgress() == -1) {
				for (Integer id : this.mShutter.getShutterIds()) {
					if (this.mShutter.getDownStatus() == OutputStatus.OFF) {
						OpenMoticsClient.getInstance().setShutter(id, false);
					} else if (this.mShutter.getDownStatus() == OutputStatus.ON) {
						OpenMoticsClient.getInstance().stopShutter(id);
					}
				}
			} else if (this.mButtonDown.getProgress() == 100) {
				for (Integer id : this.mShutter.getShutterIds()) {
					OpenMoticsClient.getInstance().stopShutter(id);
				}
			}
			this.mButtonDown.setProgress(50);
		}

		@Override
		public boolean onLongClick(View v) {
			final PopupMenu popup = new PopupMenu(v.getContext(), v);
			popup.setOnMenuItemClickListener(this);
			popup.inflate(R.menu.menu_combined_output);
			popup.show();
			return true;
		}

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			return this.mMenuItemClickListener.onMenuItemClick(item, mShutter);
		}
	}

	public interface OnOutputMenuItemClickListener {
		boolean onMenuItemClick(MenuItem item, CombinedOutput combinedOutput);
	}

	public interface OnShutterMenuItemClickListener {
		boolean onMenuItemClick(MenuItem item, CombinedShutter combinedShutter);
	}
}
