package net.rivercrew.domotics.omremote.outputs;

import net.rivercrew.domotics.omremote.OpenMoticsClient;

public class Dimmer extends SingleOutput {
	private int percentage;

	public Dimmer(int id, String name, OutputStatus status, int percentage) {
		super(id, name, status);

		this.percentage = percentage;
	}

	public void setPercentage(int percentage) {
		OpenMoticsClient.getInstance().setOutput(getId(), true, percentage);
	}

	public int getPercentage() {
		return percentage;
	}

	public void setStatus(OutputStatus status, int percentage) {
		super.setStatus(status);

		if (percentage >= 0 && percentage <= 100) {
			this.percentage = percentage;
		}
	}
}
