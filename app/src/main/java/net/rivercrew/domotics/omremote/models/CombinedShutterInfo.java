package net.rivercrew.domotics.omremote.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.rivercrew.domotics.omremote.OMRemoteDatabase;

@Table(database = OMRemoteDatabase.class)
public class CombinedShutterInfo extends BaseModel {
	@PrimaryKey(autoincrement = true)
	public int id;
	@Column
	public int combinedId;
	@Column
	public int shutterId;
	@Column
	public String name;

	public CombinedShutterInfo() {
		super();
	}

	public CombinedShutterInfo(int combinedId, String name, int shutterId) {
		super();

		this.combinedId = combinedId;
		this.name = name;
		this.shutterId = shutterId;
	}
}
