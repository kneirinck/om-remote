package net.rivercrew.domotics.omremote.floors;

import android.annotation.SuppressLint;

import net.rivercrew.domotics.omremote.FloorFragment;
import net.rivercrew.domotics.omremote.OpenMoticsApi;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.api.OutputInfosConverter;
import net.rivercrew.domotics.omremote.outputs.DeprecatedShutter;
import net.rivercrew.domotics.omremote.outputs.Dimmer;
import net.rivercrew.domotics.omremote.outputs.Output;
import net.rivercrew.domotics.omremote.outputs.OutputStatus;
import net.rivercrew.domotics.omremote.outputs.Shutter;
import net.rivercrew.domotics.omremote.outputs.SingleOutput;

import java.util.HashMap;
import java.util.Set;

public class Floors {
	@SuppressLint("UseSparseArrays")
	private static final HashMap<Integer, Floor> floors = new HashMap<>();

	private Floors() {
	}

	public static Floor getFloor(int id) {
		return floors.get(id);
	}

	public static Set<Integer> getFloorIds() {
		return floors.keySet();
	}

	public static void clear() {
		floors.clear();
	}

	public static void update(HashMap<Integer, Floor> newFloors) {
		floors.putAll(newFloors);
	}

	public static void updateOutputStatus(int id, OpenMoticsClient.OutputInfo.Status outputInfoStatus, int dimmerPercentage) {
		OutputStatus status = OutputInfosConverter.toOutputStatus(outputInfoStatus);

		for (Floor floor : floors.values()) {
			for (int i = 0; i < floor.getOutputCount(); ++i) {
				Output output = floor.getOutputAt(i);
				if (output instanceof DeprecatedShutter) {
					DeprecatedShutter shutter = (DeprecatedShutter) output;
					if (shutter.getUpId() == id) {
						shutter.setUpStatus(status);
						return;
					} else if (shutter.getDownId() == id) {
						shutter.setDownStatus(status);
						return;
					}
				} else {
					if (output.getId() == id) {
						if (output instanceof Dimmer) {
							((Dimmer) output).setStatus(status, dimmerPercentage);
						} else {
							((SingleOutput) output).setStatus(status);
						}
						return;
					}
				}
			}
		}
	}

	public static void updateShutterStatus(int id, OpenMoticsApi.ShutterStatus shutterStatus) {
		Floor floor = floors.get(FloorFragment.SHUTTERS_FLOOR_NUMBER);
		if (floor == null) {
			return;
		}

		for (int i = 0; i < floor.getOutputCount(); ++i) {
			Output output = floor.getOutputAt(i);
			if (output instanceof Shutter && output.hasId(id)) {
				Shutter shutter = (Shutter) output;
				shutter.setStatus(shutterStatus);
			}
		}
	}
}
