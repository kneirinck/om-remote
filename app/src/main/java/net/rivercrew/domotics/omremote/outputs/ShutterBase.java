package net.rivercrew.domotics.omremote.outputs;

public interface ShutterBase {
	OutputStatus getUpStatus();

	OutputStatus getDownStatus();

	void toggleUp();

	void toggleDown();
}
