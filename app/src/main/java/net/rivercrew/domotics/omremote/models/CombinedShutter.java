package net.rivercrew.domotics.omremote.models;

import android.database.Cursor;
import android.util.SparseArray;

import com.raizlabs.android.dbflow.sql.language.Method;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import net.rivercrew.domotics.omremote.OpenMoticsApi;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.outputs.OutputStatus;

import java.util.ArrayList;
import java.util.HashSet;

public class CombinedShutter {
	private final ArrayList<CombinedShutterInfo> mInfos;
	private final String mName;
	private final SparseArray<OpenMoticsApi.ShutterStatus> mStatuses;

	public CombinedShutter(String name, OpenMoticsClient.ShutterInfos shutterInfos) {
		this.mName = name;
		this.mInfos = new ArrayList<>(shutterInfos.size());
		this.mStatuses = new SparseArray<>();

		// Get the next combined id.
		final int combinedId;
		final Cursor cursor = SQLite.select(Method.max(CombinedShutterInfo_Table.combinedId)).from(CombinedShutterInfo.class).query();
		if (cursor != null && cursor.moveToFirst()) {
			combinedId = cursor.getInt(0) + 1;
		} else {
			combinedId = 1;
		}

		// Add all rows
		this.addShutters(combinedId, shutterInfos);
	}

	private CombinedShutter(ArrayList<CombinedShutterInfo> infos) {
		this.mInfos = infos;
		this.mName = infos.get(0).name;
		this.mStatuses = new SparseArray<>();
	}

	public String getName() {
		return mName;
	}

	public boolean updateStatus(int shutterId, OpenMoticsApi.ShutterStatus status) {
		boolean updated = false;
		for (CombinedShutterInfo info : this.mInfos) {
			if (info.shutterId == shutterId) {
				updated = true;
				this.mStatuses.put(shutterId, status);
			}
		}
		return updated;
	}

	public ArrayList<Integer> getShutterIds() {
		final ArrayList<Integer> ids = new ArrayList<>(this.mInfos.size());
		for (CombinedShutterInfo info : this.mInfos) {
			ids.add(info.shutterId);
		}
		return ids;
	}

	public int getCombinedId() {
		if (this.mInfos.isEmpty()) {
			return 0;
		}
		return this.mInfos.get(0).combinedId;
	}

	public void update(OpenMoticsClient.ShutterInfos shutterInfos) {
		final int combinedId = this.getCombinedId();
		this.mInfos.clear();
		this.mStatuses.clear();
		this.addShutters(combinedId, shutterInfos);
		CombinedShutter.deleteFromDatabase(this);
		this.saveToDatabase();
	}

	public void saveToDatabase() {
		for (CombinedShutterInfo info : this.mInfos) {
			info.save();
		}
	}

	private void addShutters(int combinedId, OpenMoticsClient.ShutterInfos shutterInfos) {
		for (OpenMoticsClient.ShutterInfo info : shutterInfos) {
			final CombinedShutterInfo combinedShutterInfo = new CombinedShutterInfo(combinedId, this.mName, info.id);
			this.mInfos.add(combinedShutterInfo);
			this.mStatuses.put(info.id, info.status);
		}
	}

	public static ArrayList<CombinedShutter> getAllFromDatabase() {
		final ArrayList<CombinedShutter> combinedShutters = new ArrayList<>();
		ArrayList<CombinedShutterInfo> shutterInfos = new ArrayList<>();
		for (CombinedShutterInfo outputInfo : SQLite.select().from(CombinedShutterInfo.class).orderBy(CombinedShutterInfo_Table.combinedId,
			true).queryList()) {
			if (!shutterInfos.isEmpty() && shutterInfos.get(0).combinedId != outputInfo.combinedId) {
				combinedShutters.add(new CombinedShutter(shutterInfos));
				shutterInfos = new ArrayList<>();
			}
			shutterInfos.add(outputInfo);
		}
		if (!shutterInfos.isEmpty()) {
			combinedShutters.add(new CombinedShutter(shutterInfos));
		}
		return combinedShutters;
	}

	public static void deleteFromDatabase(CombinedShutter combinedShutter) {
		SQLite.delete().from(CombinedShutterInfo.class).where(CombinedShutterInfo_Table.combinedId.eq(combinedShutter.getCombinedId())).execute();
	}

	public OutputStatus getUpStatus() {
		final HashSet<OutputStatus> statusSet = new HashSet<>();
		for (CombinedShutterInfo info : this.mInfos) {
			statusSet.add(this.mStatuses.get(info.shutterId, OpenMoticsApi.ShutterStatus.UNKNOWN).upStatus());
		}
		if (statusSet.size() != 1) {
			// Unknown status, some are on, some are off.
			statusSet.clear();
			statusSet.add(OutputStatus.UNKNOWN);
		}
		return statusSet.iterator().next();
	}

	public OutputStatus getDownStatus() {
		final HashSet<OutputStatus> statusSet = new HashSet<>();
		for (CombinedShutterInfo info : this.mInfos) {
			statusSet.add(this.mStatuses.get(info.shutterId, OpenMoticsApi.ShutterStatus.UNKNOWN).downStatus());
		}
		if (statusSet.size() != 1) {
			// Unknown status, some are on, some are off.
			statusSet.clear();
			statusSet.add(OutputStatus.UNKNOWN);
		}
		return statusSet.iterator().next();
	}
}
