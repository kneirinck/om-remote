package net.rivercrew.domotics.omremote;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import net.rivercrew.domotics.omremote.adapters.OutputSelectionAdapter;
import net.rivercrew.domotics.omremote.models.CombinedShutter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CombinedShutterDialog {
	private final View mView;
	@BindView(R.id.name)
	protected EditText mName;
	private final OutputSelectionAdapter<OpenMoticsClient.ShutterInfo, OpenMoticsClient.ShutterInfos> mAdapter;

	public static void show(Context context, final CombinedShutterDialog.DialogConfirmedListener listener) {
		show(context, listener, null);
	}

	public static void show(Context context, final CombinedShutterDialog.DialogConfirmedListener listener, final CombinedShutter combinedShutter) {
		final CombinedShutterDialog combinedDialog = new CombinedShutterDialog(context, combinedShutter);

		new AlertDialog.Builder(context)
			.setPositiveButton(combinedShutter == null ? R.string.add : R.string.save, (dialog, id) -> {
				// Add a combined output.
				listener.onDialogConfirmed(combinedShutter,
					combinedDialog.getName(),
					combinedDialog.mAdapter.getSelectedItems(new OpenMoticsClient.ShutterInfos()));
			})
			.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.dismiss())
			.setView(combinedDialog.getView())
			.create()
			.show();
	}

	public CombinedShutterDialog(Context context, CombinedShutter combinedShutter) {
		this.mView = LayoutInflater.from(context).inflate(R.layout.dialog_combined_output, null);
		ButterKnife.bind(this, this.mView);
		if (combinedShutter != null) {
			mName.setText(combinedShutter.getName());
		}

		OpenMoticsClient.ShutterInfos infos = OpenMoticsClient.getInstance().getLast(OpenMoticsClient.ShutterInfos.class);

		if (infos == null) {
			infos = new OpenMoticsClient.ShutterInfos();
		}

		this.mAdapter = new OutputSelectionAdapter<>(infos, combinedShutter == null ? new ArrayList<>() : combinedShutter.getShutterIds());
		final RecyclerView list = (RecyclerView) this.mView.findViewById(R.id.list);
		list.setAdapter(this.mAdapter);
		list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
	}

	private View getView() {
		return this.mView;
	}

	private String getName() {
		final String name = this.mName.getText().toString();
		if (!name.isEmpty()) {
			return name;
		}
		return this.mName.getHint().toString();
	}

	public interface DialogConfirmedListener {
		void onDialogConfirmed(CombinedShutter combinedShutter, String name, OpenMoticsClient.ShutterInfos infos);
	}
}
