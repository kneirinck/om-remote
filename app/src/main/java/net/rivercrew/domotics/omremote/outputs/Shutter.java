package net.rivercrew.domotics.omremote.outputs;

import net.rivercrew.domotics.omremote.OpenMoticsApi;
import net.rivercrew.domotics.omremote.OpenMoticsClient;

public class Shutter extends Output implements ShutterBase {
	private OpenMoticsApi.ShutterStatus status;

	public Shutter(int id, String name, OpenMoticsApi.ShutterStatus status) {
		super(id, name);
		this.status = status;
	}

	@Override
	public OutputStatus getUpStatus() {
		return status.upStatus();
	}

	@Override
	public OutputStatus getDownStatus() {
		return status.downStatus();
	}

	@Override
	public void toggleUp() {
		if (getUpStatus() == OutputStatus.OFF) {
			OpenMoticsClient.getInstance().setShutter(getId(), true);
		} else if (getUpStatus() == OutputStatus.ON) {
			OpenMoticsClient.getInstance().stopShutter(getId());
		}
	}

	@Override
	public void toggleDown() {
		if (getDownStatus() == OutputStatus.OFF) {
			OpenMoticsClient.getInstance().setShutter(getId(), false);
		} else if (getDownStatus() == OutputStatus.ON) {
			OpenMoticsClient.getInstance().stopShutter(getId());
		}
	}

	public void setStatus(OpenMoticsApi.ShutterStatus status) {
		this.status = status;
	}
}
