package net.rivercrew.domotics.omremote.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.SettingsActivity;
import net.rivercrew.domotics.omremote.widgets.WidgetHelper;

import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class AutoRefreshTimer extends Timer implements SharedPreferences.OnSharedPreferenceChangeListener {
	private final SharedPreferences mPreferences;
	private final Subscription mSubscription;
	private final int mDefaultRefreshInterval;
	private final RefreshType mRefreshType;
	private boolean mLocalInfoComplete;
	private boolean mRemoteInfoComplete;
	private long mInterval;

	public AutoRefreshTimer(final Context context) {
		this(context, RefreshType.DEFAULT);
	}

	public AutoRefreshTimer(final Context context, RefreshType refreshType) {
		super(TimeUnit.SECONDS.toMillis(context.getResources().getInteger(R.integer.default_auto_refresh_interval)), Timer.DURATION_INFINITY);

		this.mRefreshType = refreshType;

		this.mDefaultRefreshInterval = context.getResources().getInteger(R.integer.default_auto_refresh_interval);
		this.mInterval = TimeUnit.SECONDS.toMillis(mDefaultRefreshInterval);

		this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		this.mPreferences.registerOnSharedPreferenceChangeListener(this);

		this.mLocalInfoComplete = verifyInfo(true);
		this.mRemoteInfoComplete = verifyInfo(false);

		// Subscribe for output info changes (gets called each tick)
		this.mSubscription = OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.OutputInfos.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(new Action1<OpenMoticsClient.OutputInfos>() {
				@Override
				public void call(OpenMoticsClient.OutputInfos outputInfos) {
					// Loop over widgets and find the correct output info
					WidgetHelper.updateAllWidgets(context);
				}
			});

		this.updateInterval();
		this.start();

		// Force a tick to start immediately.
		this.onTick();
	}

	@Override
	protected void onTick() {
		switch (this.mRefreshType) {
			case ENERGY:
				OpenMoticsClient.getInstance().refreshEnergyData(mRemoteInfoComplete);
				break;
			default:
			case DEFAULT:
				OpenMoticsClient.getInstance().refresh(mLocalInfoComplete, mRemoteInfoComplete);
				break;
		}
	}

	@Override
	protected void onFinish() {
		mSubscription.unsubscribe();
	}

	@Override
	public void cancel() {
		super.cancel();

		this.mPreferences.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(SettingsActivity.PREF_APP_AUTO_REFRESH_INTERVAL)) {
			this.pause();
			this.updateInterval();
			this.start();
		} else if (key.equals(SettingsActivity.PREF_LOCAL_IP) ||
			key.equals(SettingsActivity.PREF_LOCAL_PASSWORD) ||
			key.equals(SettingsActivity.PREF_LOCAL_USERNAME)) {

			this.mLocalInfoComplete = verifyInfo(true);
		} else if (key.equals(SettingsActivity.PREF_REMOTE_PASSWORD) ||
			key.equals(SettingsActivity.PREF_REMOTE_USERNAME)) {

			this.mRemoteInfoComplete = verifyInfo(false);
		}
	}

	@Override
	public void setInterval(long interval) {
		this.mInterval = interval;

		super.setInterval(interval);
	}

	@Override
	public void start() {
		if (this.mInterval > 0) {
			super.start();
		}
	}

	private void updateInterval() {
		final String intervalString = this.mPreferences.getString(
			SettingsActivity.PREF_APP_AUTO_REFRESH_INTERVAL,
			Integer.toString(this.mDefaultRefreshInterval)
		);
		if (TextUtils.isDigitsOnly(intervalString)) {
			this.setInterval(TimeUnit.SECONDS.toMillis(Integer.valueOf(intervalString)));
		}
	}

	private boolean verifyInfo(boolean localInfo) {
		if (localInfo) {
			return (!TextUtils.isEmpty(this.mPreferences.getString(SettingsActivity.PREF_LOCAL_IP, "")) &&
				!TextUtils.isEmpty(this.mPreferences.getString(SettingsActivity.PREF_LOCAL_PASSWORD, "")) &&
				!TextUtils.isEmpty(this.mPreferences.getString(SettingsActivity.PREF_LOCAL_USERNAME, "")));
		} else {
			return (!TextUtils.isEmpty(this.mPreferences.getString(SettingsActivity.PREF_REMOTE_PASSWORD, "")) &&
				!TextUtils.isEmpty(this.mPreferences.getString(SettingsActivity.PREF_REMOTE_USERNAME, "")));
		}
	}

	public enum RefreshType {
		DEFAULT,
		ENERGY
	}
}
