package net.rivercrew.domotics.omremote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import net.rivercrew.domotics.omremote.adapters.CombinedAdapter;
import net.rivercrew.domotics.omremote.adapters.OutputAdapter;
import net.rivercrew.domotics.omremote.models.CombinedOutput;
import net.rivercrew.domotics.omremote.models.CombinedShutter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FloorFragment extends Fragment implements CombinedOutputDialog.DialogConfirmedListener, CombinedAdapter.OnOutputMenuItemClickListener, CombinedShutterDialog.DialogConfirmedListener, CombinedAdapter.OnShutterMenuItemClickListener {
	public final static String ARG_FLOOR = "floor";
	public final static int COMBINED_FLOOR_NUMBER = Integer.MAX_VALUE;
	public final static int UNASSIGNED_FLOOR_NUMBER = Integer.MAX_VALUE - 1;
	public final static int SHUTTERS_FLOOR_NUMBER = Integer.MAX_VALUE - 2;

	@BindView(R.id.list)
	protected RecyclerView mList;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.item_floor, container, false);

		ButterKnife.bind(this, view);
		this.mList.setLayoutManager(new LinearLayoutManager(this.getContext()));
		final int floorNumber = this.getArguments().getInt(ARG_FLOOR, 0);
		if (floorNumber != COMBINED_FLOOR_NUMBER) {
			final ItemTouchHelper touchHelper = new ItemTouchHelper(new OutputItemTouchHelper());
			touchHelper.attachToRecyclerView(this.mList);
			this.mList.setAdapter(new OutputAdapter(floorNumber, touchHelper));
		} else {
			this.mList.setAdapter(new CombinedAdapter(this, this));
		}

		return view;
	}

	@Override
	public void onDestroy() {
		if (this.mList != null && this.mList.getAdapter() != null) {
			final RecyclerView.Adapter adapter = this.mList.getAdapter();
			if (adapter instanceof IUnsubscribable) {
				((IUnsubscribable) adapter).unsubscribe();
			}
		}
		super.onDestroy();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		if (this.mList.getAdapter() instanceof CombinedAdapter) {
			inflater.inflate(R.menu.menu_floor_combined, menu);
		} else {
			inflater.inflate(R.menu.menu_floor, menu);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.edit:
				if (this.mList.getAdapter() != null) {
					final RecyclerView.Adapter adapter = this.mList.getAdapter();
					if (adapter instanceof ActionMode.Callback) {
						((MainActivity) this.getActivity()).startSupportActionMode((ActionMode.Callback) adapter);
					}
				}
				return true;
			case R.id.add:
				new AlertDialog.Builder(getContext())
					.setMessage(R.string.combined_output_add_message)
					.setNegativeButton(R.string.shutters, (dialogInterface, i) -> CombinedShutterDialog.show(getContext(), this))
					.setPositiveButton(R.string.outputs, (dialogInterface, i) -> CombinedOutputDialog.show(getContext(), this))
					.create()
					.show();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDialogConfirmed(CombinedOutput combinedOutput, String name, OpenMoticsClient.OutputInfos infos) {
		if (infos.isEmpty()) {
			return;
		}

		final CombinedAdapter adapter = (CombinedAdapter) this.mList.getAdapter();
		if (combinedOutput != null) {
			combinedOutput.update(infos);
			adapter.update(combinedOutput);
		} else {
			combinedOutput = new CombinedOutput(name, infos);
			adapter.add(combinedOutput);
			combinedOutput.saveToDatabase();
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item, CombinedOutput combinedOutput) {
		// Popup menu item selected
		switch (item.getItemId()) {
			case R.id.edit:
				CombinedOutputDialog.show(this.getContext(), this, combinedOutput);
				return true;
			case R.id.delete:
				CombinedOutput.deleteFromDatabase(combinedOutput);
				((CombinedAdapter) this.mList.getAdapter()).delete(combinedOutput);
				return true;
			default:
				return false;
		}
	}

	@Override
	public void onDialogConfirmed(CombinedShutter combinedShutter, String name, OpenMoticsClient.ShutterInfos infos) {
		if (infos.isEmpty()) {
			return;
		}

		final CombinedAdapter adapter = (CombinedAdapter) this.mList.getAdapter();
		if (combinedShutter != null) {
			combinedShutter.update(infos);
			adapter.update(combinedShutter);
		} else {
			combinedShutter = new CombinedShutter(name, infos);
			adapter.add(combinedShutter);
			combinedShutter.saveToDatabase();
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item, CombinedShutter combinedShutter) {
		// Popup menu item selected
		switch (item.getItemId()) {
			case R.id.edit:
				CombinedShutterDialog.show(this.getContext(), this, combinedShutter);
				return true;
			case R.id.delete:
				CombinedShutter.deleteFromDatabase(combinedShutter);
				((CombinedAdapter) this.mList.getAdapter()).delete(combinedShutter);
				return true;
			default:
				return false;
		}
	}
}
