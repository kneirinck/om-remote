package net.rivercrew.domotics.omremote.outputs;

public abstract class Output {
	private final int id;
	private final String name;

	Output(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public boolean hasId(int id) {
		return this.id == id;
	}

	public String getName() {
		return name;
	}

}
