package net.rivercrew.domotics.omremote.api;

import android.annotation.SuppressLint;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.floors.Floor;
import net.rivercrew.domotics.omremote.outputs.Output;
import net.rivercrew.domotics.omremote.outputs.Shutter;

import java.util.ArrayList;
import java.util.HashMap;

public class ShutterInfosConverter {
	private ShutterInfosConverter() {
	}

	@SuppressLint("UseSparseArrays")
	public static HashMap<Integer, Floor> toFloors(ArrayList<OpenMoticsClient.ShutterInfo> shutterInfos) {
		ArrayList<Output> shutters = new ArrayList<>(shutterInfos.size());
		for (OpenMoticsClient.ShutterInfo info : shutterInfos) {
			shutters.add(new Shutter(info.id, info.name, info.status));
		}

		HashMap<Integer, Floor> floors = new HashMap<>(1);
		floors.put(Integer.MAX_VALUE - 2, new Floor(shutters));
		return floors;
	}
}
