package net.rivercrew.domotics.omremote.outputs;

public class DeprecatedShutter extends Output implements ShutterBase {
	private final DeprecatedShutterPart up;
	private final DeprecatedShutterPart down;

	public DeprecatedShutter(DeprecatedShutterPart up, DeprecatedShutterPart down) {
		super(up.getId(), up.getName().substring(0, up.getName().length() - 3));

		this.up = up;
		this.down = down;
	}

	@Override
	public OutputStatus getUpStatus() {
		return up.getStatus();
	}

	@Override
	public OutputStatus getDownStatus() {
		return down.getStatus();
	}

	@Override
	public void toggleUp() {
		up.toggle();
	}

	@Override
	public void toggleDown() {
		down.toggle();
	}

	public int getUpId() {
		return up.getId();
	}

	public int getDownId() {
		return down.getId();
	}

	public void setUpStatus(OutputStatus status) {
		up.setStatus(status);
		if (status == OutputStatus.ON) {
			down.setStatus(OutputStatus.OFF);
		}
	}

	public void setDownStatus(OutputStatus status) {
		down.setStatus(status);
		if (status == OutputStatus.ON) {
			up.setStatus(OutputStatus.OFF);
		}
	}

	@Override
	public boolean hasId(int id) {
		return getUpId() == id || getDownId() == id;
	}
}
