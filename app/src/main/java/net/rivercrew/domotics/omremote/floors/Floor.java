package net.rivercrew.domotics.omremote.floors;


import android.annotation.SuppressLint;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.outputs.Output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Floor {
	private final ArrayList<Output> outputs;
	private ArrayList<Output> movedOutputs;

	@SuppressLint("UseSparseArrays")
	public Floor(ArrayList<Output> outputs) {
		this.outputs = outputs;

		// Read order from database
		HashMap<Integer, Integer> outputOrderMap = new HashMap<>();
		final List<OpenMoticsClient.OutputOrder> outputOrders = SQLite.select()
			.from(OpenMoticsClient.OutputOrder.class)
			.queryList();
		for (OpenMoticsClient.OutputOrder order : outputOrders) {
			outputOrderMap.put(order.outputId, order.order);
		}

		Collections.sort(this.outputs, (Output lhs, Output rhs) -> {
			Integer lhsOrder = outputOrderMap.get(lhs.getId());
			if (lhsOrder == null) {
				lhsOrder = Integer.MAX_VALUE;
			}
			Integer rhsOrder = outputOrderMap.get(rhs.getId());
			if (rhsOrder == null) {
				rhsOrder = Integer.MAX_VALUE;
			}
			if (lhsOrder != Integer.MAX_VALUE || rhsOrder != Integer.MAX_VALUE) {
				return lhsOrder.compareTo(rhsOrder);
			} else {
				// Fall back to alphabetical ordering.
				return lhs.getName().compareToIgnoreCase(rhs.getName());
			}
		});

	}

	public void moveOutput(int fromPosition, int toPosition) {
		if (movedOutputs == null) {
			movedOutputs = new ArrayList<>(outputs);
		}

		Collections.swap(movedOutputs, fromPosition, toPosition);
	}

	public void confirmMoves() {
		// Store the order in the database
		for (int i = 0; i < movedOutputs.size(); ++i) {
			OpenMoticsClient.OutputOrder order = new OpenMoticsClient.OutputOrder(movedOutputs.get(i).getId());
			order.order = i;
			order.save();
		}

		outputs.clear();
		outputs.addAll(movedOutputs);
	}

	public void cancelMoves() {
		movedOutputs = null;
	}

	public int getOutputCount() {
		return outputs.size();
	}

	public Output getOutputAt(int position) {
		if (movedOutputs != null) {
			return movedOutputs.get(position);
		}
		return outputs.get(position);
	}
}
