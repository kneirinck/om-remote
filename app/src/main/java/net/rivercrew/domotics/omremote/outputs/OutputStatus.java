package net.rivercrew.domotics.omremote.outputs;


public enum OutputStatus {
	ON,
	OFF,
	UNKNOWN
}
