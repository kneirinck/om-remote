package net.rivercrew.domotics.omremote.adapters;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dd.processbutton.iml.ActionProcessButton;

import net.rivercrew.domotics.omremote.FloorFragment;
import net.rivercrew.domotics.omremote.IUnsubscribable;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.floors.Floor;
import net.rivercrew.domotics.omremote.floors.Floors;
import net.rivercrew.domotics.omremote.outputs.DeprecatedShutter;
import net.rivercrew.domotics.omremote.outputs.DeprecatedShutterPart;
import net.rivercrew.domotics.omremote.outputs.Dimmer;
import net.rivercrew.domotics.omremote.outputs.Light;
import net.rivercrew.domotics.omremote.outputs.Output;
import net.rivercrew.domotics.omremote.outputs.OutputStatus;
import net.rivercrew.domotics.omremote.outputs.Relay;
import net.rivercrew.domotics.omremote.outputs.Shutter;
import net.rivercrew.domotics.omremote.outputs.ShutterBase;
import net.rivercrew.domotics.omremote.outputs.SingleOutput;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class OutputAdapter extends RecyclerView.Adapter<OutputAdapter.ViewHolder> implements Action1<Object>, ActionMode.Callback, IUnsubscribable {
	private static final int TYPE_SINGLE_OUTPUT = 0;
	private static final int TYPE_SHUTTER = 1;
	private static final int TYPE_DIMMER = 2;

	private final ArrayList<Subscription> mSubscriptions;
	private final ItemTouchHelper mTouchHelper;
	private final int mFloorId;

	private boolean mReordering;
	private Floor mFloor;

	public OutputAdapter(int floor, ItemTouchHelper touchHelper) {
		this.mFloorId = floor;
		updateFloor();
		this.mSubscriptions = new ArrayList<>();
		this.mTouchHelper = touchHelper;

		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.OutputInfos.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));

		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.ShutterInfos.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));

		this.mSubscriptions.add(OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsClient.OutputStatus.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this));

		setHasStableIds(true);
	}

	private void updateFloor() {
		this.mFloor = Floors.getFloor(this.mFloorId);
		if (this.mFloor == null) {
			this.mFloor = new Floor(new ArrayList<>());
		}
	}

	@Override
	public void unsubscribe() {
		for (Subscription subscription : this.mSubscriptions) {
			subscription.unsubscribe();
		}
	}

	@Override
	public int getItemViewType(int position) {
		Output output = this.mFloor.getOutputAt(position);
		if (output instanceof DeprecatedShutter || output instanceof Shutter) {
			return TYPE_SHUTTER;
		} else if (output instanceof Dimmer) {
			return TYPE_DIMMER;
		} else {
			return TYPE_SINGLE_OUTPUT;
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		switch (viewType) {
			case TYPE_SHUTTER:
				return new ShutterViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shutter, parent, false));
			case TYPE_DIMMER:
				return new DimmerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dimmer, parent, false));
			case TYPE_SINGLE_OUTPUT:
			default:
				return new SingleOutputViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_output, parent, false));
		}
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bind(this.mFloor.getOutputAt(position));
	}

	@Override
	public int getItemCount() {
		return this.mFloor.getOutputCount();
	}

	@Override
	public long getItemId(int position) {
		return this.mFloor.getOutputAt(position).getId();
	}

	@Override
	public void call(Object o) {
		if (o instanceof OpenMoticsClient.OutputInfos || o instanceof OpenMoticsClient.ShutterInfos) {
			this.updateFloor();
			this.notifyDataSetChanged();
		} else if (o instanceof OpenMoticsClient.OutputStatus) {
			final OpenMoticsClient.OutputStatus status = (OpenMoticsClient.OutputStatus) o;
			if (status.ofShutter && this.mFloorId != FloorFragment.SHUTTERS_FLOOR_NUMBER) {
				return;
			}
			for (int i = 0; i < this.mFloor.getOutputCount(); ++i) {
				if (this.mFloor.getOutputAt(i).hasId(status.id)) {
					this.notifyItemChanged(i);
					break;
				}
			}
		}
	}

	//region ActionMode/Ordering
	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		mode.getMenuInflater().inflate(R.menu.menu_floor_edit, menu);
		this.mReordering = true;
		this.notifyDataSetChanged();
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		switch (item.getItemId()) {
			case R.id.save:
				this.mFloor.confirmMoves();
				mode.finish();
				break;
		}
		return false;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
		this.mReordering = false;
		this.mFloor.cancelMoves();
		this.notifyDataSetChanged();
	}

	public void onItemMove(int fromPosition, int toPosition) {
		this.mFloor.moveOutput(fromPosition, toPosition);
		notifyItemMoved(fromPosition, toPosition);
	}
	//endregion

	//region ViewHolders
	protected abstract class ViewHolder extends RecyclerView.ViewHolder {
		Output output;
		@BindView(R.id.text)
		TextView mText;
		@BindView(R.id.icon)
		ImageView mIcon;
		@BindView(R.id.drag)
		ImageView mDrag;

		public ViewHolder(View view) {
			super(view);

			ButterKnife.bind(this, view);
		}

		public void bind(Output output) {
			if (output instanceof Light) {
				this.mIcon.setImageResource(R.drawable.lightbulb);
			} else if (output instanceof Relay) {
				this.mIcon.setImageResource(R.drawable.relay);
			} else if (output instanceof Dimmer) {
				this.mIcon.setImageResource(R.drawable.dimmer);
			} else if (output instanceof DeprecatedShutterPart || output instanceof DeprecatedShutter || output instanceof Shutter) {
				this.mIcon.setImageResource(R.drawable.shutter);
			} else {
				this.mIcon.setImageResource(R.drawable.unknown);
			}
			this.mText.setText(output.getName());

			this.output = output;
		}

		@OnTouch(R.id.drag)
		boolean onTouch(@SuppressWarnings("unused") View view, MotionEvent event) {
			if (!mReordering) {
				return false;
			}

			if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
				mTouchHelper.startDrag(this);
			}
			return false;
		}

		void updateButton(ActionProcessButton button, OutputStatus status) {
			switch (status) {
				case ON:
					button.setProgress(100);
					break;
				case OFF:
					button.setProgress(-1);
					break;
				case UNKNOWN:
				default:
					button.setProgress(50);
			}
		}
	}

	protected class SingleOutputViewHolder extends ViewHolder {
		@BindView(R.id.button)
		ActionProcessButton mButton;

		SingleOutputViewHolder(View view) {
			super(view);

			this.mButton.setMode(ActionProcessButton.Mode.ENDLESS);
		}

		@Override
		public void bind(Output output) {
			super.bind(output);

			updateButton(this.mButton, ((SingleOutput) this.output).getStatus());

			if (mReordering) {
				this.mDrag.setVisibility(View.VISIBLE);
				this.mButton.setVisibility(View.INVISIBLE);
			} else {
				this.mDrag.setVisibility(View.GONE);
				this.mButton.setVisibility(View.VISIBLE);
			}
		}

		@OnClick(R.id.button)
		void onClick(@SuppressWarnings("unused") View view) {
			((SingleOutput) this.output).toggle();
			this.mButton.setProgress(50);
		}

		@OnTouch(R.id.button)
		boolean onTouch(View view, MotionEvent event) {
			return super.onTouch(view, event);
		}
	}

	protected class ShutterViewHolder extends ViewHolder {
		@BindView(R.id.button_up)
		ActionProcessButton mButtonUp;
		@BindView(R.id.button_down)
		ActionProcessButton mButtonDown;

		ShutterViewHolder(View view) {
			super(view);

			this.mButtonUp.setMode(ActionProcessButton.Mode.ENDLESS);
			this.mButtonDown.setMode(ActionProcessButton.Mode.ENDLESS);
		}

		@Override
		public void bind(Output output) {
			super.bind(output);

			updateButton(this.mButtonUp, ((ShutterBase) output).getUpStatus());
			updateButton(this.mButtonDown, ((ShutterBase) output).getDownStatus());

			if (mReordering) {
				this.mDrag.setVisibility(View.VISIBLE);
				this.mButtonUp.setVisibility(View.INVISIBLE);
				this.mButtonDown.setVisibility(View.INVISIBLE);
			} else {
				this.mDrag.setVisibility(View.GONE);
				this.mButtonUp.setVisibility(View.VISIBLE);
				this.mButtonDown.setVisibility(View.VISIBLE);

			}
		}

		@OnClick(R.id.button_up)
		void onClickUp(@SuppressWarnings("unused") View view) {
			((ShutterBase) this.output).toggleUp();
			this.mButtonUp.setProgress(50);
		}

		@OnClick(R.id.button_down)
		void onClickDown(@SuppressWarnings("unused") View view) {
			((ShutterBase) this.output).toggleDown();
			this.mButtonDown.setProgress(50);
		}

		@OnTouch(R.id.button_down)
		boolean onTouch(View view, MotionEvent event) {
			return super.onTouch(view, event);
		}
	}

	protected class DimmerViewHolder extends SingleOutputViewHolder implements SeekBar.OnSeekBarChangeListener {
		private static final int STEP_SIZE = 10;

		@BindView(R.id.text_percentage)
		TextView mPercentage;
		@BindView(R.id.seek_percentage)
		SeekBar mSeekbar;

		DimmerViewHolder(View view) {
			super(view);

			this.mSeekbar.setOnSeekBarChangeListener(this);
		}

		@Override
		public void bind(Output output) {
			if (this.output == null || output.getId() != this.output.getId()) {
				setSeekbarVisibility(View.GONE);
			}

			super.bind(output);

			Dimmer dimmer = (Dimmer) output;
			this.mPercentage.setText(String.format(Locale.US, "%d%%", dimmer.getPercentage()));
			this.mSeekbar.setProgress(dimmer.getPercentage());
		}

		@OnLongClick(R.id.button)
		boolean onLongClick(@SuppressWarnings("unused") View view) {
			if (this.output instanceof Dimmer) {
				setSeekbarVisibility(View.VISIBLE);
				return true;
			}
			return false;
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			seekBar.setProgress(Math.round((float) progress / STEP_SIZE) * STEP_SIZE);
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			((Dimmer) this.output).setPercentage(seekBar.getProgress());
			this.mButton.setProgress(50);
			setSeekbarVisibility(View.GONE);
		}

		void setSeekbarVisibility(int visibility) {
			this.mSeekbar.setVisibility(visibility);
			if (visibility == View.GONE) {
				this.mIcon.setVisibility(View.VISIBLE);
				this.mText.setVisibility(View.VISIBLE);
			} else {
				this.mIcon.setVisibility(View.GONE);
				this.mText.setVisibility(View.GONE);
			}
		}
	}
	//endregion
}
