package net.rivercrew.domotics.omremote.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SwipeDisableableViewPager extends ViewPager {
	private boolean mSwipeEnabled = true;

	public SwipeDisableableViewPager(Context context) {
		super(context);
	}

	public SwipeDisableableViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void setSwipeEnabled(boolean swipeEnabled) {
		this.mSwipeEnabled = swipeEnabled;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (!this.mSwipeEnabled) {
			return false;
		}
		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (!this.mSwipeEnabled) {
			return false;
		}
		return super.onTouchEvent(ev);
	}
}
