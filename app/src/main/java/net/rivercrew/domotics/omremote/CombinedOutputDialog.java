package net.rivercrew.domotics.omremote;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import net.rivercrew.domotics.omremote.adapters.OutputSelectionAdapter;
import net.rivercrew.domotics.omremote.models.CombinedOutput;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CombinedOutputDialog {
	private final View mView;
	@BindView(R.id.name)
	protected EditText mName;
	private final OutputSelectionAdapter<OpenMoticsClient.OutputInfo, OpenMoticsClient.OutputInfos> mAdapter;

	public static void show(Context context, final DialogConfirmedListener listener) {
		show(context, listener, null);
	}

	public static void show(Context context, final DialogConfirmedListener listener, final CombinedOutput combinedOutput) {
		final CombinedOutputDialog combinedDialog = new CombinedOutputDialog(context, combinedOutput);

		new AlertDialog.Builder(context)
			.setPositiveButton(combinedOutput == null ? R.string.add : R.string.save, (dialog, id) -> {
				// Add a combined output.
				listener.onDialogConfirmed(combinedOutput,
					combinedDialog.getName(),
					combinedDialog.mAdapter.getSelectedItems(new OpenMoticsClient.OutputInfos()));
			})
			.setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.dismiss())
			.setView(combinedDialog.getView())
			.create()
			.show();
	}

	public CombinedOutputDialog(Context context, CombinedOutput combinedOutput) {
		this.mView = LayoutInflater.from(context).inflate(R.layout.dialog_combined_output, null);
		ButterKnife.bind(this, this.mView);
		if (combinedOutput != null) {
			mName.setText(combinedOutput.getName());
		}

		OpenMoticsClient.OutputInfos infos = OpenMoticsClient.getInstance().getLast(OpenMoticsClient.OutputInfos.class);

		if (infos == null) {
			infos = new OpenMoticsClient.OutputInfos();
		}

		this.mAdapter = new OutputSelectionAdapter<>(infos, combinedOutput == null ? new ArrayList<>() : combinedOutput.getOutputIds());
		final RecyclerView list = (RecyclerView) this.mView.findViewById(R.id.list);
		list.setAdapter(this.mAdapter);
		list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
	}

	private View getView() {
		return this.mView;
	}

	private String getName() {
		final String name = this.mName.getText().toString();
		if (!name.isEmpty()) {
			return name;
		}
		return this.mName.getHint().toString();
	}

	public interface DialogConfirmedListener {
		void onDialogConfirmed(CombinedOutput combinedOutput, String name, OpenMoticsClient.OutputInfos infos);
	}
}
