package net.rivercrew.domotics.omremote;

import android.content.Context;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.widget.Toast;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.rivercrew.domotics.omremote.api.OutputInfosConverter;
import net.rivercrew.domotics.omremote.api.ShutterInfosConverter;
import net.rivercrew.domotics.omremote.floors.Floors;
import net.rivercrew.domotics.omremote.utils.RxThreadingCallAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.security.auth.login.LoginException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Observable;
import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;

public class OpenMoticsClient {
	private static OpenMoticsClient sInstance;
	private static final String CLOUD_HOST = "cloud.openmotics.com";

	private OpenMoticsApi mApi;
	private OpenMoticsApi mLocalApi;
	private final OpenMoticsApi mRemoteApi;
	private final HashMap<Class, BehaviorSubject<Object>> mBus;
	private final Action1<Throwable> mErrorHandler;
	private SparseArray<OpenMoticsApi.OutputConfiguration> mOutputConfigs;
	private SparseArray<OpenMoticsApi.ShutterConfiguration> mShutterConfigs;
	private boolean mDebug;
	private int subscriberId = -1;
	private int messageId = -1;

	private OpenMoticsClient(final Context context) {
		this.mErrorHandler = throwable -> {
			if (!mDebug) {
				return;
			}
			final String message;
			if (throwable instanceof LoginException) {
				// Login failed
				message = "Local login failed";
			} else if (throwable instanceof HttpException && ((HttpException) throwable).code() == TokenProviderInterceptor.CODE_LOGIN_FAILED) {
				if (((HttpException) throwable).response().raw().request().url().host().equals(CLOUD_HOST)) {
					message = "Remote login failed";
				} else {
					message = "Local login failed";
				}
			} else {
				final String localizedMessage = throwable.getLocalizedMessage();
				if (TextUtils.isEmpty(localizedMessage)) {
					return;
				}
				message = context.getString(R.string.network_error, localizedMessage);
			}
			Log.e("OpenMoticsClient", message, throwable);
			Toast.makeText(
				context,
				message,
				Toast.LENGTH_SHORT
			).show();
		};

		// Read the debug setting
		this.mDebug = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingsActivity.PREF_APP_DEBUG, false);

		this.mLocalApi = OpenMoticsClient.createApi(context, true);
		this.mRemoteApi = OpenMoticsClient.createApi(context, false);
		this.mApi = this.mRemoteApi; // Default to remote so we already have something to call
		this.checkConnectivity(true);

		// Create a behavior subject to always get the last value when subscribing.
		// http://reactivex.io/documentation/subject.html
		this.mBus = new HashMap<>();
		this.mBus.put(OutputStatus.class, BehaviorSubject.create(new Object()));
		this.mBus.put(OutputInfos.class, BehaviorSubject.create(new Object()));
		this.mBus.put(ShutterInfos.class, BehaviorSubject.create(new Object()));
		this.mBus.put(Floors.class, BehaviorSubject.create(new Object()));
		this.mBus.put(OpenMoticsApi.MultipleMessages.class, BehaviorSubject.create(new Object()));

		// Load initial data from db.
		final List<OpenMoticsApi.OutputConfiguration> outputConfigurations = SQLite.select()
			.from(OpenMoticsApi.OutputConfiguration.class)
			.queryList();
		final List<OpenMoticsApi.ShutterConfiguration> shutterConfigurations = SQLite.select()
			.from(OpenMoticsApi.ShutterConfiguration.class)
			.queryList();
		this.mOutputConfigs = new SparseArray<>(outputConfigurations.size());
		this.mShutterConfigs = new SparseArray<>(outputConfigurations.size());
		this.updateMemoryConfigCache(outputConfigurations, shutterConfigurations, false);

		Floors.clear();
		final OutputInfos outputInfos = new OutputInfos(outputConfigurations.size());
		for (OpenMoticsApi.OutputConfiguration config : outputConfigurations) {
			final OutputInfo info = new OutputInfo();
			info.parse(config);
			outputInfos.add(info);
		}
		Floors.update(OutputInfosConverter.toFloors(outputInfos));

		final ShutterInfos shutterInfos = new ShutterInfos(shutterConfigurations.size());
		for (OpenMoticsApi.ShutterConfiguration config : shutterConfigurations) {
			final ShutterInfo info = new ShutterInfo();
			info.parse(config);
			shutterInfos.add(info);
		}
		Floors.update(ShutterInfosConverter.toFloors(shutterInfos));

		// Notify subscribers.
		this.mBus.get(OutputInfos.class).onNext(outputInfos);
		this.mBus.get(ShutterInfos.class).onNext(shutterInfos);
		this.mBus.get(Floors.class).onNext(new Object());
	}

	public static void create(Context context) {
		if (sInstance == null) {
			sInstance = new OpenMoticsClient(context);
		}
	}

	public static OpenMoticsClient getInstance() {
		return sInstance;
	}

	private static OpenMoticsApi createApi(Context context, boolean isLocal) {
		final OkHttpClient.Builder okHttpClientBuilder;
		if (isLocal) {
			okHttpClientBuilder = UnsafeHttpsClient.getUnsafeOkHttpClientBuilder();
			okHttpClientBuilder.connectTimeout(1500, TimeUnit.MILLISECONDS);
		} else {
			okHttpClientBuilder = new OkHttpClient.Builder();
		}
		if (BuildConfig.DEBUG) {
			okHttpClientBuilder.addNetworkInterceptor(new StethoInterceptor());
		}
		final TokenProviderInterceptor tokenInterceptor = new TokenProviderInterceptor(
			PreferenceManager.getDefaultSharedPreferences(context),
			isLocal
		);
		okHttpClientBuilder.addInterceptor(tokenInterceptor);

		final String baseUrl;
		if (isLocal) {
			baseUrl = "https://" + PreferenceManager.getDefaultSharedPreferences(context).getString(SettingsActivity.PREF_LOCAL_IP, "127.0.0.1");
		} else {
			baseUrl = "https://" + CLOUD_HOST + "/api/";
		}

		final OpenMoticsApi api = new Retrofit.Builder()
			.baseUrl(baseUrl)
			.addConverterFactory(ScalarsConverterFactory.create())
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxThreadingCallAdapterFactory.create())
			.client(okHttpClientBuilder.build())
			.build()
			.create(OpenMoticsApi.class);

		tokenInterceptor.setApi(api);
		return api;
	}

	private void updateMemoryConfigCache(List<OpenMoticsApi.OutputConfiguration> outputConfigurations, List<OpenMoticsApi.ShutterConfiguration> shutterConfigurations, boolean save) {
		for (OpenMoticsApi.OutputConfiguration config : outputConfigurations) {
			this.mOutputConfigs.put(config.id, config);
			if (save) {
				// Save to db.
				config.save();
			}
		}

		for (OpenMoticsApi.ShutterConfiguration config : shutterConfigurations) {
			this.mShutterConfigs.put(config.id, config);
			if (save) {
				// Save to db.
				config.save();
			}
		}

	}

	private OutputInfos getOutputInfos(List<OpenMoticsApi.OutputStatus> statuses) {
		final OutputInfos infos = new OutputInfos(statuses.size());
		for (OpenMoticsApi.OutputStatus status : statuses) {
			final OutputInfo info = new OutputInfo();
			info.parse(status);
			final OpenMoticsApi.OutputConfiguration config = this.mOutputConfigs.get(status.id);
			if (config == null) {
				continue;
			}
			info.parse(config);
			infos.add(info);
		}
		return infos;
	}

	private ShutterInfos getShutterInfos(List<OpenMoticsApi.ShutterStatus> statuses) {
		final ShutterInfos infos = new ShutterInfos(statuses.size());
		for (int id = 0; id < statuses.size(); ++id) {
			final ShutterInfo info = new ShutterInfo();
			info.parse(statuses.get(id));
			final OpenMoticsApi.ShutterConfiguration config = this.mShutterConfigs.get(id);
			if (config == null) {
				continue;
			}
			info.parse(config);
			infos.add(info);
		}
		return infos;
	}

	private int getCurrentTime() {
		return (int) TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
	}

	private void getEnergyData() {
		this.mRemoteApi.getMessages(this.subscriberId, this.getCurrentTime(), this.messageId)
			.subscribe((multipleMessages) -> {
				this.messageId = multipleMessages.lastMessageId;
				this.mBus.get(OpenMoticsApi.MultipleMessages.class).onNext(multipleMessages);
			}, this.mErrorHandler);
	}

	public void fullRefresh() {
		Observable.zip(this.mApi.getOutputStatus(),
			this.mApi.getShutterStatus(),
			this.mApi.getOutputConfigurations(),
			this.mApi.getShutterConfigurations(),
			(outputStatusResult, shutterStatusResult, outputConfigResult, shutterConfigResult) -> {
				updateMemoryConfigCache(outputConfigResult.config, shutterConfigResult.config, true);
				return Pair.create(getOutputInfos(outputStatusResult.status), getShutterInfos(shutterStatusResult.status));
			}
		).subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(outputAndShutterInfos -> {
				// Notify subscribers.
				Floors.clear();
				Floors.update(OutputInfosConverter.toFloors(outputAndShutterInfos.first));
				Floors.update(ShutterInfosConverter.toFloors(outputAndShutterInfos.second));
				mBus.get(OutputInfos.class).onNext(outputAndShutterInfos.first);
				mBus.get(ShutterInfos.class).onNext(outputAndShutterInfos.second);
				mBus.get(Floors.class).onNext(new Object());
			}, this.mErrorHandler);
	}

	public void refresh() {
		this.mApi.getOutputStatus()
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(outputStatusResult -> {
				// Notify subscribers.
				final OutputInfos outputInfos = getOutputInfos(outputStatusResult.status);
				Floors.update(OutputInfosConverter.toFloors(outputInfos));
				mBus.get(OutputInfos.class).onNext(outputInfos);
			}, this.mErrorHandler);

		this.mApi.getShutterStatus()
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(shutterStatusesResult -> {
				// Notify subscribers.
				final ShutterInfos shutterInfos = getShutterInfos(shutterStatusesResult.status);
				Floors.update(ShutterInfosConverter.toFloors(shutterInfos));
				mBus.get(ShutterInfos.class).onNext(shutterInfos);
			}, this.mErrorHandler);
	}

	public void refresh(boolean allowLocal, boolean allowRemote) {
		if ((this.mApi == this.mLocalApi && allowLocal) || (this.mApi == this.mRemoteApi && allowRemote)) {
			this.refresh();
		}
	}

	/// Subscribe to specific events on the bus.
	public <T> Observable<T> subscribeFor(final Class<T> eventType) {
		return this.mBus.get(eventType).ofType(eventType).onBackpressureBuffer();
	}

	public <T> Observable<Object> subscribeForIgnoringType(final Class<T> eventType) {
		return this.mBus.get(eventType).onBackpressureBuffer();
	}

	public <T> T getLast(final Class<T> eventType) {
		final Iterator<T> iterator = this.mBus.get(eventType).ofType(eventType).toBlocking().latest().iterator();
		if (iterator.hasNext()) {
			return iterator.next();
		}
		return null;
	}

	public Single<OutputStatus> setOutput(final int id, final boolean on) {
		return setOutput(id, on, Integer.MAX_VALUE);
	}

	public Single<OutputStatus> setOutput(final int id, final boolean on, final int dimmerPercentage) {
		final PublishSubject<OutputStatus> subject = PublishSubject.create();
		Observable<OpenMoticsApi.Result> outputResult;
		if (dimmerPercentage < 0 || dimmerPercentage > 100) {
			outputResult = this.mApi.setOutput(id, on);
		} else {
			outputResult = this.mApi.setOutput(id, on, dimmerPercentage);
		}
		outputResult.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((OpenMoticsApi.Result result) -> {
				// Notify subscribers.
				final OutputStatus status = new OutputStatus(id, false);
				if (result.success) {
					if (on) {
						status.status = OutputInfo.Status.ON;
					} else {
						status.status = OutputInfo.Status.OFF;
					}
				} else {
					if (on) {
						status.status = OutputInfo.Status.OFF;
					} else {
						status.status = OutputInfo.Status.ON;
					}
				}
				Floors.updateOutputStatus(status.id, status.status, dimmerPercentage);
				mBus.get(OutputStatus.class).onNext(status);
				subject.onNext(status);
				subject.onCompleted();
			}, this.mErrorHandler);
		return subject.onBackpressureBuffer().toSingle();
	}

	public Single<OutputStatus> setShutter(final int id, final boolean up) {
		final PublishSubject<OutputStatus> subject = PublishSubject.create();
		Observable<OpenMoticsApi.Result> shutterResult;
		if (up) {
			shutterResult = this.mApi.shutterUp(id);
		} else {
			shutterResult = this.mApi.shutterDown(id);
		}
		shutterResult.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((OpenMoticsApi.Result result) -> {
				// Notify subscribers.
				final OutputStatus status = new OutputStatus(id, true);
				if (result.success) {
					status.status = OutputInfo.Status.ON;
					Floors.updateShutterStatus(status.id, up ? OpenMoticsApi.ShutterStatus.GOING_UP : OpenMoticsApi.ShutterStatus.GOING_DOWN);
				} else {
					status.status = OutputInfo.Status.UNKNOWN;
					Floors.updateShutterStatus(status.id, OpenMoticsApi.ShutterStatus.UNKNOWN);
				}
				mBus.get(OutputStatus.class).onNext(status);
				subject.onNext(status);
				subject.onCompleted();
			}, this.mErrorHandler);
		return subject.onBackpressureBuffer().toSingle();
	}

	public Single<OutputStatus> stopShutter(final int id) {
		final PublishSubject<OutputStatus> subject = PublishSubject.create();
		this.mApi.shutterStop(id).subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe((OpenMoticsApi.Result result) -> {
				// Notify subscribers.
				final OutputStatus status = new OutputStatus(id, true);
				if (result.success) {
					status.status = OutputInfo.Status.OFF;
					Floors.updateShutterStatus(status.id, OpenMoticsApi.ShutterStatus.STOPPED);
				} else {
					status.status = OutputInfo.Status.UNKNOWN;
					Floors.updateShutterStatus(status.id, OpenMoticsApi.ShutterStatus.UNKNOWN);
				}
				mBus.get(OutputStatus.class).onNext(status);
				subject.onNext(status);
				subject.onCompleted();
			}, this.mErrorHandler);
		return subject.onBackpressureBuffer().toSingle();
	}

	/// Check whether the local api is reachable or not.
	public void checkConnectivity() {
		this.checkConnectivity(false);
	}

	/// Check whether the local api is reachable or not.
	public void checkConnectivity(final boolean forceRefresh) {
		this.mLocalApi.getStatus().enqueue(new Callback<OpenMoticsApi.Result>() {
			@Override
			public void onResponse(Call<OpenMoticsApi.Result> call, Response<OpenMoticsApi.Result> response) {
				if (!response.isSuccessful()) {
					if (response.code() == TokenProviderInterceptor.CODE_LOGIN_FAILED) {
						// Login failure
						onFailure(call, new LoginException());
					} else {
						onFailure(call, new Throwable());
					}
					return;
				}
				mApi = mLocalApi;
				if (forceRefresh) {
					refresh();
				}
			}

			@Override
			public void onFailure(Call<OpenMoticsApi.Result> call, Throwable t) {
				mErrorHandler.call(t);
				mApi = mRemoteApi;
				if (forceRefresh) {
					refresh();
				}
			}
		});
	}

	public void updateLocalIpAddress(Context context) {
		mLocalApi = createApi(context, true);
		checkConnectivity(true);
	}

	public void setDebug(boolean enabled) {
		mDebug = enabled;
	}

	public void refreshEnergyData(boolean allowRemote) {
		if (!allowRemote) {
			return;
		}

		if (this.subscriberId == -1) {
			final Random random = new Random();
			this.subscriberId = random.nextInt(1000000);

			this.mRemoteApi.updateMessageSubscription(
				this.subscriberId,
				this.getCurrentTime(),
				new Gson().toJson(Collections.singletonList(OpenMoticsApi.MessageType.MSG_RT_ENERGY_TAGS))
			).subscribe((result) -> {
				this.mRemoteApi.getLastMessageId(this.subscriberId, this.getCurrentTime())
					.subscribe((messageId) -> {
						this.messageId = messageId;
						this.getEnergyData();
					}, this.mErrorHandler);
			}, this.mErrorHandler);
		} else {
			this.getEnergyData();
		}
	}

	public static abstract class BaseOutputInfo {
		public String name;
		public int id;
	}

	public static class OutputInfo extends BaseOutputInfo {
		public enum Status {
			ON,
			OFF,
			UNKNOWN
		}

		public Status status = Status.UNKNOWN;
		public byte floor;
		public OpenMoticsApi.ModuleType moduleType;
		public byte type;
		public int dimmer;

		public void parse(OpenMoticsApi.OutputConfiguration config) {
			this.id = config.id;
			this.floor = config.floor;
			this.name = config.name;
			this.moduleType = config.moduleType;
			this.type = config.type;
		}

		public void parse(OpenMoticsApi.OutputStatus status) {
			this.id = status.id;
			if (status.status == 0) {
				this.status = Status.OFF;
			} else {
				this.status = Status.ON;
			}
			this.dimmer = status.dimmer;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static class OutputInfos extends ArrayList<OutputInfo> {
		public OutputInfos() {
			super();
		}

		public OutputInfos(Collection<? extends OutputInfo> collection) {
			super(collection);
		}

		public OutputInfos(int capacity) {
			super(capacity);
		}
	}

	public static class OutputStatus {
		public final int id;
		public final boolean ofShutter;
		public OutputInfo.Status status;

		public OutputStatus(int id, boolean ofShutter) {
			this.id = id;
			this.ofShutter = ofShutter;
		}
	}

	public static class ShutterInfo extends BaseOutputInfo {
		public OpenMoticsApi.ShutterStatus status = OpenMoticsApi.ShutterStatus.UNKNOWN;

		public void parse(OpenMoticsApi.ShutterConfiguration config) {
			this.id = config.id;
			this.name = config.name;
		}

		public void parse(OpenMoticsApi.ShutterStatus status) {
			this.status = status;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static class ShutterInfos extends ArrayList<ShutterInfo> {
		public ShutterInfos() {
			super();
		}

		public ShutterInfos(Collection<? extends ShutterInfo> collection) {
			super(collection);
		}

		public ShutterInfos(int capacity) {
			super(capacity);
		}
	}

	@Table(database = OMRemoteDatabase.class)
	public static class OutputOrder extends BaseModel {
		@PrimaryKey
		public int outputId;

		@Column
		public int order;

		public OutputOrder() {
			super();

			this.order = Integer.MAX_VALUE;
		}

		public OutputOrder(int id) {
			this();

			this.outputId = id;
		}
	}
}
