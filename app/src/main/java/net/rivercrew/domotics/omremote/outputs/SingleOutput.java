package net.rivercrew.domotics.omremote.outputs;


import net.rivercrew.domotics.omremote.OpenMoticsClient;

public abstract class SingleOutput extends Output {
	private OutputStatus status;

	public SingleOutput(int id, String name, OutputStatus status) {
		super(id, name);

		this.status = status;
	}

	public OutputStatus getStatus() {
		return status;
	}

	public void toggle() {
		if (status == OutputStatus.OFF) {
			OpenMoticsClient.getInstance().setOutput(getId(), true);
		} else if (status == OutputStatus.ON) {
			OpenMoticsClient.getInstance().setOutput(getId(), false);
		}
	}

	public void setStatus(OutputStatus status) {
		this.status = status;
	}
}
