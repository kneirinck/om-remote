package net.rivercrew.domotics.omremote.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.rivercrew.domotics.omremote.EnergyActivity;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.SettingsActivity;

public class DrawerAdapter extends ArrayAdapter<String> implements AdapterView.OnItemClickListener {
	public DrawerAdapter(Context context) {
		super(context, android.R.layout.simple_list_item_1);

		this.add(context.getString(R.string.settings));
		this.add(context.getString(R.string.full_refresh));
		this.add(context.getString(R.string.energy_data));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final TextView view = (TextView) super.getView(position, convertView, parent);
		view.setTextColor(Color.WHITE);
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		switch (position) {
			case 0:
				this.getContext().startActivity(new Intent(this.getContext(), SettingsActivity.class));
				break;
			case 1:
				OpenMoticsClient.getInstance().fullRefresh();
				break;
			case 2:
				this.getContext().startActivity(new Intent(this.getContext(), EnergyActivity.class));
				break;
		}
	}
}
