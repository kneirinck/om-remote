package net.rivercrew.domotics.omremote;

import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

public class Application extends android.app.Application {
	@Override
	public void onCreate() {
		super.onCreate();
		FlowManager.init(new FlowConfig.Builder(this).openDatabasesOnInit(true).build());

		if (BuildConfig.DEBUG) {
			Stetho.initializeWithDefaults(this);
		}
		OpenMoticsClient.create(this);
		Foreground.init(this);
	}
}
