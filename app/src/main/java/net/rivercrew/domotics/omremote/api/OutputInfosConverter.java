package net.rivercrew.domotics.omremote.api;


import android.annotation.SuppressLint;

import com.google.common.collect.Iterables;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.floors.Floor;
import net.rivercrew.domotics.omremote.outputs.DeprecatedShutter;
import net.rivercrew.domotics.omremote.outputs.DeprecatedShutterPart;
import net.rivercrew.domotics.omremote.outputs.Dimmer;
import net.rivercrew.domotics.omremote.outputs.Light;
import net.rivercrew.domotics.omremote.outputs.Output;
import net.rivercrew.domotics.omremote.outputs.OutputStatus;
import net.rivercrew.domotics.omremote.outputs.Relay;

import java.util.ArrayList;
import java.util.HashMap;

public class OutputInfosConverter {
	private OutputInfosConverter() {
	}

	@SuppressLint("UseSparseArrays")
	public static HashMap<Integer, Floor> toFloors(ArrayList<OpenMoticsClient.OutputInfo> outputInfos) {
		HashMap<Byte, ArrayList<Output>> groupedOutputs = new HashMap<>();
		for (OpenMoticsClient.OutputInfo info : outputInfos) {
			if (groupedOutputs.get(info.floor) == null) {
				groupedOutputs.put(info.floor, new ArrayList<>());
			}
			Output output = toOutput(info);
			if (output != null) {
				groupedOutputs.get(info.floor).add(toOutput(info));
			}
		}

		HashMap<Integer, Floor> floors = new HashMap<>(groupedOutputs.size());
		for (HashMap.Entry<Byte, ArrayList<Output>> entry : groupedOutputs.entrySet()) {
			// Create Shutters
			ArrayList<Output> outputs = entry.getValue();
			createShutters(outputs);

			floors.put(Integer.valueOf(entry.getKey()), new Floor(outputs));
		}
		return floors;
	}

	private static void createShutters(ArrayList<Output> outputs) {
		for (Output output : new ArrayList<>(outputs)) { // Use a copy to iterate so we can adjust the original list
			if (!(output instanceof DeprecatedShutterPart) || output.getId() % 2 != 0) {
				continue;
			}

			// Find counterpart
			Output part2 = Iterables.find(outputs, (Output o) -> {
				assert o != null;
				return o.getId() == output.getId() + 1;
			});

			if (part2 == null) {
				continue;
			}

			DeprecatedShutterPart up;
			DeprecatedShutterPart down;
			if (output.getName().endsWith("UP")) {
				up = (DeprecatedShutterPart) output;
				down = (DeprecatedShutterPart) part2;
			} else {
				up = (DeprecatedShutterPart) part2;
				down = (DeprecatedShutterPart) output;
			}

			outputs.remove(up);
			outputs.remove(down);
			outputs.add(new DeprecatedShutter(up, down));
		}
	}

	private static Output toOutput(OpenMoticsClient.OutputInfo info) {
		switch (info.moduleType) {
			case OUTPUT:
				if (info.type != 0) {
					return new Light(info.id, info.name, toOutputStatus(info.status));
				} else {
					return new Relay(info.id, info.name, toOutputStatus(info.status));
				}
			case ROLLER:
				return new DeprecatedShutterPart(info.id, info.name, toOutputStatus(info.status));
			case DIMMER:
				return new Dimmer(info.id, info.name, toOutputStatus(info.status), info.dimmer);
			default:
				return null;
		}
	}

	public static OutputStatus toOutputStatus(OpenMoticsClient.OutputInfo.Status status) {
		switch (status) {
			case ON:
				return OutputStatus.ON;
			case OFF:
				return OutputStatus.OFF;
			case UNKNOWN:
			default:
				return OutputStatus.UNKNOWN;
		}
	}
}
