package net.rivercrew.domotics.omremote.widgets;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

import net.rivercrew.domotics.omremote.BuildConfig;
import net.rivercrew.domotics.omremote.Foreground;
import net.rivercrew.domotics.omremote.OpenMoticsClient;

import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class WidgetProvider extends AppWidgetProvider {
	public static final String ACTION_TOGGLE_ON = BuildConfig.APPLICATION_ID + ".ACTION_TOGGLE_ON";
	public static final String ACTION_TOGGLE_OFF = BuildConfig.APPLICATION_ID + ".ACTION_TOGGLE_OFF";
	public static final String ACTION_UPDATE_ALL = BuildConfig.APPLICATION_ID + ".ACTION_UPDATE_ALL";
	public static final String EXTRA_WIDGET_ID = "widgetId";
	public static final String EXTRA_OUTPUT_ID = "outputId";

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// Perform this loop procedure for each App Widget that belongs to this provider
		for (final int appWidgetId : appWidgetIds) {
			WidgetHelper.updateWidget(context, appWidgetId, WidgetHelper.loadWidget(context, appWidgetId));
		}
	}

	@Override
	public void onReceive(final Context context, final Intent intent) {
		super.onReceive(context, intent);

		if (intent.getAction().equals(ACTION_TOGGLE_OFF) || intent.getAction().equals(ACTION_TOGGLE_ON)) {
			final int outputId = intent.getIntExtra(EXTRA_OUTPUT_ID, -1);
			if (outputId != -1) {
				final boolean on = intent.getAction().equals(ACTION_TOGGLE_ON);
				OpenMoticsClient.getInstance().setOutput(outputId, on)
					.subscribeOn(Schedulers.io())
					.subscribe(new Action1<OpenMoticsClient.OutputStatus>() {
						@Override
						public void call(OpenMoticsClient.OutputStatus status) {
							WidgetHelper.updateWidgetStatus(context, intent.getIntExtra(EXTRA_WIDGET_ID, -1), status);
						}
					});
			}
		} else if (intent.getAction().equals(ACTION_UPDATE_ALL)) {
			// Only force a refresh when the app isn't in the foreground.
			// While it's in the foreground it gets refreshed through the AutoRefreshTimer.
			if (!Foreground.get().isForeground()) {
				// The AutoRefreshTimer subscription will update the widgets.
				OpenMoticsClient.getInstance().refresh();
			}
		}
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		for (final int appWidgetId : appWidgetIds) {
			WidgetHelper.deleteWidget(context, appWidgetId);
		}
	}

	// Called when the first widget gets created
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);

		WidgetHelper.updateAlarm(context);
	}

	// Called when the last widget gets removed
	@Override
	public void onDisabled(Context context) {
		super.onDisabled(context);

		WidgetHelper.cancelAlarm(context);
	}
}
