package net.rivercrew.domotics.omremote.widgets;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

import net.rivercrew.domotics.omremote.BuildConfig;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.SettingsActivity;

import java.util.concurrent.TimeUnit;

public class WidgetHelper {
	private static final String PREFS_NAME = BuildConfig.APPLICATION_ID + ".widgets";
	private static final String PREF_PREFIX_KEY = "widget_";

	private WidgetHelper() {
	}

	public static void updateWidget(Context context, int appWidgetId, OpenMoticsClient.OutputInfo widgetOutputInfo) {
		if (widgetOutputInfo == null) {
			return;
		}

		WidgetHelper.updateWidgetStatus(context, appWidgetId, widgetOutputInfo, widgetOutputInfo.status);
	}

	public static void updateWidgetStatus(Context context, int appWidgetId, OpenMoticsClient.OutputStatus widgetOutputStatus) {
		if (appWidgetId == -1) {
			return;
		}

		OpenMoticsClient.OutputInfo widgetOutputInfo = null;
		for (OpenMoticsClient.OutputInfo outputInfo : OpenMoticsClient.getInstance().getLast(OpenMoticsClient.OutputInfos.class)) {
			if (outputInfo.id == widgetOutputStatus.id) {
				widgetOutputInfo = outputInfo;
				break;
			}
		}
		if (widgetOutputInfo == null) {
			return;
		}
		WidgetHelper.updateWidgetStatus(context, appWidgetId, widgetOutputInfo, widgetOutputStatus.status);
	}

	public static void updateAllWidgets(final Context context) {
		final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		for (int appWidgetId : appWidgetManager.getAppWidgetIds(new ComponentName(context, WidgetProvider.class))) {
			final OpenMoticsClient.OutputInfo widgetOutputInfo = WidgetHelper.loadWidget(context, appWidgetId);

			if (widgetOutputInfo != null) {
				updateWidget(context, appWidgetId, widgetOutputInfo);
			}
		}
	}

	public static void saveWidget(Context context, int widgetId, int outputId) {
		context
			.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
			.edit()
			.putInt(PREF_PREFIX_KEY + widgetId, outputId)
			.apply();
	}

	public static OpenMoticsClient.OutputInfo loadWidget(Context context, int widgetId) {
		final int outputId = context
			.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
			.getInt(PREF_PREFIX_KEY + widgetId, -1);

		for (OpenMoticsClient.OutputInfo outputInfo : OpenMoticsClient.getInstance().getLast(OpenMoticsClient.OutputInfos.class)) {
			if (outputInfo.id == outputId) {
				return outputInfo;
			}
		}
		return null;
	}

	public static void deleteWidget(Context context, int widgetId) {
		context
			.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
			.edit()
			.remove(PREF_PREFIX_KEY + widgetId)
			.apply();
	}

	// Register/update our update event
	public static void updateAlarm(Context context) {
		final long interval = TimeUnit.SECONDS.toMillis(
			Long.valueOf(
				PreferenceManager.getDefaultSharedPreferences(context).getString(
					SettingsActivity.PREF_APP_WIDGET_REFRESH_INTERVAL,
					Integer.toString(context.getResources().getInteger(R.integer.default_widget_refresh_interval))
				)
			)
		);

		((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).setRepeating(
			AlarmManager.RTC,
			System.currentTimeMillis() + interval,
			interval,
			WidgetHelper.createUpdateAlarmPendingIntent(context)
		);
	}

	// Cancel our update event
	public static void cancelAlarm(Context context) {
		((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).cancel(WidgetHelper.createUpdateAlarmPendingIntent(context));
	}

	private static void updateWidgetStatus(final Context context, final int appWidgetId, OpenMoticsClient.OutputInfo outputInfo, OpenMoticsClient.OutputInfo.Status status) {
		if (appWidgetId == -1) {
			return;
		}

		// Get the layout for the App Widget
		final RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);
		remoteViews.setTextViewText(R.id.text, outputInfo.name);
		if (status != OpenMoticsClient.OutputInfo.Status.UNKNOWN) {
			// Create an Intent for on click
			final Intent intent = new Intent(context, WidgetProvider.class);
			intent.setAction((status == OpenMoticsClient.OutputInfo.Status.OFF ? WidgetProvider.ACTION_TOGGLE_ON : WidgetProvider.ACTION_TOGGLE_OFF));
			intent.putExtra(WidgetProvider.EXTRA_WIDGET_ID, appWidgetId);
			intent.putExtra(WidgetProvider.EXTRA_OUTPUT_ID, outputInfo.id);
			remoteViews.setOnClickPendingIntent(R.id.circle, PendingIntent.getBroadcast(context, appWidgetId, intent, 0));
		} else {
			// Remove on click
			remoteViews.setOnClickPendingIntent(R.id.circle, null);
		}

		final int resourceId;
		switch (status) {
			case OFF:
				resourceId = R.drawable.widget_circle_off;
				break;
			case ON:
				resourceId = R.drawable.widget_circle_on;
				break;
			default:
			case UNKNOWN:
				resourceId = R.drawable.widget_circle_unknown;
				break;
		}
		remoteViews.setImageViewResource(R.id.circle, resourceId);

		final int typeId;
		switch (outputInfo.moduleType) {
			case OUTPUT:
				if (outputInfo.type != 0) {
					typeId = R.drawable.lightbulb;
				} else {
					typeId = R.drawable.relay;
				}
				break;
			case DIMMER:
				typeId = R.drawable.dimmer;
				break;
			case ROLLER:
				typeId = R.drawable.shutter;
				break;
			default:
				typeId = R.drawable.unknown;
		}
		remoteViews.setImageViewResource(R.id.type, typeId);

		// Tell the AppWidgetManager to perform an update on the current app widget
		AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, remoteViews);
	}

	private static PendingIntent createUpdateAlarmPendingIntent(Context context) {
		final Intent updateIntent = new Intent(context, WidgetProvider.class);
		updateIntent.setAction(WidgetProvider.ACTION_UPDATE_ALL);
		return PendingIntent.getBroadcast(context, 0, updateIntent, PendingIntent.FLAG_CANCEL_CURRENT);
	}
}
