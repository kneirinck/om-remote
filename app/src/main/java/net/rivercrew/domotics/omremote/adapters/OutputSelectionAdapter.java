package net.rivercrew.domotics.omremote.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OutputSelectionAdapter<T extends OpenMoticsClient.BaseOutputInfo, V extends ArrayList<T>> extends RecyclerView.Adapter<OutputSelectionAdapter<T, V>.ViewHolder> {
	private final V mOutputInfos;
	private final SparseBooleanArray mSelectedItems;
	private final boolean mSingleSelectOnly;

	public OutputSelectionAdapter(V outputInfos, ArrayList<Integer> selectedIds) {
		this(outputInfos, selectedIds, false);
	}

	public OutputSelectionAdapter(V outputInfos, ArrayList<Integer> selectedIds, boolean singleSelectOnly) {
		this.mOutputInfos = outputInfos;
		this.mSelectedItems = new SparseBooleanArray();
		this.mSingleSelectOnly = singleSelectOnly;

		for (int i = 0; i < outputInfos.size(); ++i) {
			final T outputInfo = outputInfos.get(i);
			if (selectedIds.contains(outputInfo.id)) {
				this.mSelectedItems.put(i, true);
			}
		}
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dialog_output, parent, false));
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bind(this.mOutputInfos.get(position));
	}

	@Override
	public int getItemCount() {
		return this.mOutputInfos.size();
	}

	public V getSelectedItems(V selected) {
		for (int i = 0; i < this.getItemCount(); ++i) {
			if (this.mSelectedItems.get(i, false)) {
				selected.add(this.mOutputInfos.get(i));
			}
		}
		return selected;
	}

	protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		@BindView(R.id.item)
		protected CheckedTextView mItem;

		public ViewHolder(View view) {
			super(view);

			ButterKnife.bind(this, view);
		}

		public void bind(T info) {
			this.mItem.setText(info.name);
			this.mItem.setOnClickListener(this);
			this.mItem.setChecked(OutputSelectionAdapter.this.mSelectedItems.get(getLayoutPosition(), false));
		}

		@OnClick(R.id.item)
		public void onClick(View view) {
			if (OutputSelectionAdapter.this.mSingleSelectOnly) {
				// Deselect all others first
				for (int i = 0; i < OutputSelectionAdapter.this.getItemCount(); ++i) {
					if (OutputSelectionAdapter.this.mSelectedItems.get(i, false)) {
						OutputSelectionAdapter.this.mSelectedItems.put(i, false);
						OutputSelectionAdapter.this.notifyItemChanged(i);
					}
				}
			}

			this.mItem.toggle();
			OutputSelectionAdapter.this.mSelectedItems.put(getLayoutPosition(), this.mItem.isChecked());
		}
	}
}
