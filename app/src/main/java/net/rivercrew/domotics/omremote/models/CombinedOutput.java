package net.rivercrew.domotics.omremote.models;

import android.database.Cursor;
import android.util.SparseArray;

import com.raizlabs.android.dbflow.sql.language.Method;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import net.rivercrew.domotics.omremote.OpenMoticsClient;

import java.util.ArrayList;
import java.util.HashSet;

public class CombinedOutput {
	private final ArrayList<CombinedOutputInfo> mInfos;
	private final String mName;
	private final SparseArray<OpenMoticsClient.OutputInfo.Status> mStatuses;

	public CombinedOutput(String name, OpenMoticsClient.OutputInfos outputInfos) {
		this.mName = name;
		this.mInfos = new ArrayList<>(outputInfos.size());
		this.mStatuses = new SparseArray<>();

		// Get the next combined id.
		final int combinedId;
		final Cursor cursor = SQLite.select(Method.max(CombinedOutputInfo_Table.combinedId)).from(CombinedOutputInfo.class).query();
		if (cursor != null && cursor.moveToFirst()) {
			combinedId = cursor.getInt(0) + 1;
		} else {
			combinedId = 1;
		}

		// Add all rows
		this.addOutputs(combinedId, outputInfos);
	}

	private CombinedOutput(ArrayList<CombinedOutputInfo> infos) {
		this.mInfos = infos;
		this.mName = infos.get(0).name;
		this.mStatuses = new SparseArray<>();
	}

	public String getName() {
		return mName;
	}

	public boolean updateStatus(int outputId, OpenMoticsClient.OutputInfo.Status status) {
		boolean updated = false;
		for (CombinedOutputInfo info : this.mInfos) {
			if (info.outputId == outputId) {
				updated = true;
				this.mStatuses.put(outputId, status);
			}
		}
		return updated;
	}

	public OpenMoticsClient.OutputInfo.Status getStatus() {
		final HashSet<OpenMoticsClient.OutputInfo.Status> statusSet = new HashSet<>();
		for (CombinedOutputInfo info : this.mInfos) {
			statusSet.add(this.mStatuses.get(info.outputId, OpenMoticsClient.OutputInfo.Status.UNKNOWN));
		}
		if (statusSet.size() != 1) {
			// Unknown status, some are on, some are off.
			statusSet.clear();
			statusSet.add(OpenMoticsClient.OutputInfo.Status.UNKNOWN);
		}
		return statusSet.iterator().next();
	}

	public ArrayList<Integer> getOutputIds() {
		final ArrayList<Integer> ids = new ArrayList<>(this.mInfos.size());
		for (CombinedOutputInfo info : this.mInfos) {
			ids.add(info.outputId);
		}
		return ids;
	}

	public int getCombinedId() {
		if (this.mInfos.isEmpty()) {
			return 0;
		}
		return this.mInfos.get(0).combinedId;
	}

	public void update(OpenMoticsClient.OutputInfos outputInfos) {
		final int combinedId = this.getCombinedId();
		this.mInfos.clear();
		this.mStatuses.clear();
		this.addOutputs(combinedId, outputInfos);
		CombinedOutput.deleteFromDatabase(this);
		this.saveToDatabase();
	}

	public void saveToDatabase() {
		for (CombinedOutputInfo info : this.mInfos) {
			info.save();
		}
	}

	private void addOutputs(int combinedId, OpenMoticsClient.OutputInfos outputInfos) {
		for (OpenMoticsClient.OutputInfo info : outputInfos) {
			final CombinedOutputInfo combinedOutputInfo = new CombinedOutputInfo(combinedId, this.mName, info.id);
			this.mInfos.add(combinedOutputInfo);
			this.mStatuses.put(info.id, info.status);
		}
	}

	public static ArrayList<CombinedOutput> getAllFromDatabase() {
		final ArrayList<CombinedOutput> combinedOutputs = new ArrayList<>();
		ArrayList<CombinedOutputInfo> outputInfos = new ArrayList<>();
		for (CombinedOutputInfo outputInfo : SQLite.select().from(CombinedOutputInfo.class).orderBy(CombinedOutputInfo_Table.combinedId,
			true).queryList()) {
			if (!outputInfos.isEmpty() && outputInfos.get(0).combinedId != outputInfo.combinedId) {
				combinedOutputs.add(new CombinedOutput(outputInfos));
				outputInfos = new ArrayList<>();
			}
			outputInfos.add(outputInfo);
		}
		if (!outputInfos.isEmpty()) {
			combinedOutputs.add(new CombinedOutput(outputInfos));
		}
		return combinedOutputs;
	}

	public static void deleteFromDatabase(CombinedOutput combinedOutput) {
		SQLite.delete().from(CombinedOutputInfo.class).where(CombinedOutputInfo_Table.combinedId.eq(combinedOutput.getCombinedId())).execute();
	}
}
