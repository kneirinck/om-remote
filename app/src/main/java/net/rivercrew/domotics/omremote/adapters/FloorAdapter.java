package net.rivercrew.domotics.omremote.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import net.rivercrew.domotics.omremote.FloorFragment;
import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.floors.Floors;

import java.util.Locale;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class FloorAdapter extends FragmentStatePagerAdapter implements Action1<Object> {
	private final SparseArray<FloorInfo> mFloors = new SparseArray<>();
	private final SparseArray<FloorInfo> mNewFloors = new SparseArray<>();
	private final Subscription mSubscription;
	private boolean mDataSetChanging = false;

	public FloorAdapter(FragmentManager fm) {
		super(fm);

		this.mSubscription = OpenMoticsClient.getInstance()
			.subscribeForIgnoringType(Floors.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(this);
	}

	public void unsubscribe() {
		this.mSubscription.unsubscribe();
	}

	@Override
	public Fragment getItem(int position) {
		final Fragment fragment = new FloorFragment();
		final Bundle args = new Bundle();
		args.putInt(FloorFragment.ARG_FLOOR, this.mFloors.valueAt(position).number);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public int getCount() {
		return this.mFloors.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return this.mFloors.valueAt(position).name;
	}

	@Override
	public void call(Object ignore) {
		// Fill newFloors so we can compare with the current floors.
		this.mNewFloors.clear();
		for (int floorId : Floors.getFloorIds()) {
			if (this.mNewFloors.get(floorId) == null) {
				final FloorInfo floor = new FloorInfo();
				floor.number = floorId;
				if (floorId == -1) {
					floor.name = "Unassigned";
					this.mNewFloors.put(FloorFragment.UNASSIGNED_FLOOR_NUMBER, floor);
				} else {
					if (floorId != FloorFragment.SHUTTERS_FLOOR_NUMBER) {
						floor.name = String.format(Locale.ENGLISH, "Floor %d", floorId);
					} else {
						floor.name = "Shutters";
					}
					this.mNewFloors.put(floorId, floor);
				}
			}
		}

		final FloorInfo combined = new FloorInfo();
		combined.number = FloorFragment.COMBINED_FLOOR_NUMBER;
		combined.name = "Combined";
		this.mNewFloors.put(combined.number, combined);

		// Compare newFloors with the current floors.
		boolean different = false;
		if (this.mNewFloors.size() != this.mFloors.size()) {
			different = true;
		} else {
			for (int i = 0; i < this.mFloors.size(); ++i) {
				if (this.mFloors.keyAt(i) != this.mNewFloors.keyAt(i)) {
					different = true;
					break;
				}
			}
		}

		if (different) {
			// Copy over the new items.
			this.mFloors.clear();
			for (int i = 0; i < this.mNewFloors.size(); ++i) {
				this.mFloors.put(this.mNewFloors.keyAt(i), this.mNewFloors.valueAt(i));
			}

			// Set a flag so we can force a refresh.
			this.mDataSetChanging = true;
			this.notifyDataSetChanged();
			this.mDataSetChanging = false;
		}
	}

	@Override
	public int getItemPosition(Object object) {
		if (this.mDataSetChanging) {
			// Force a refresh.
			return POSITION_NONE;
		}
		return super.getItemPosition(object);
	}

	private static class FloorInfo {
		public int number;
		public String name;
	}
}
