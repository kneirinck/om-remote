package net.rivercrew.domotics.omremote.widgets;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import net.rivercrew.domotics.omremote.OpenMoticsClient;
import net.rivercrew.domotics.omremote.R;
import net.rivercrew.domotics.omremote.adapters.OutputSelectionAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WidgetConfigureActivity extends Activity implements View.OnClickListener {
	@BindView(R.id.list)
	protected RecyclerView mList;
	private OutputSelectionAdapter<OpenMoticsClient.OutputInfo, OpenMoticsClient.OutputInfos> mAdapter;

	private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Set the result to CANCELED.  This will cause the widget host to cancel
		// out of the widget placement if they press the back button.
		this.setResult(RESULT_CANCELED);

		// Set the view layout resource to use.
		this.setContentView(R.layout.activity_widget_configure);

		ButterKnife.bind(this);

		// Find the widget id from the intent.
		final Intent intent = this.getIntent();
		final Bundle extras = intent.getExtras();
		if (extras != null) {
			this.mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		// If they gave us an intent without the widget id, just bail.
		if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			this.finish();
		}

		OpenMoticsClient.OutputInfos infos = OpenMoticsClient.getInstance().getLast(OpenMoticsClient.OutputInfos.class);
		if (infos == null) {
			infos = new OpenMoticsClient.OutputInfos();
		}
		this.mAdapter = new OutputSelectionAdapter<>(infos, new ArrayList<Integer>(), true);
		this.mList.setAdapter(this.mAdapter);
		this.mList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
	}

	@OnClick(R.id.button_save)
	@Override
	public void onClick(View v) {
		if (this.mAdapter.getSelectedItems(new OpenMoticsClient.OutputInfos()).isEmpty()) {
			this.finish();
			return;
		}

		// Save preferences
		final OpenMoticsClient.OutputInfo widgetOutputInfo = this.mAdapter.getSelectedItems(new OpenMoticsClient.OutputInfos()).get(0);
		WidgetHelper.saveWidget(this, mAppWidgetId, widgetOutputInfo.id);

		// Force widget update
		WidgetHelper.updateWidget(this, mAppWidgetId, widgetOutputInfo);

		// Make sure we pass back the original appWidgetId
		final Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
		this.setResult(RESULT_OK, resultValue);
		this.finish();
	}
}
