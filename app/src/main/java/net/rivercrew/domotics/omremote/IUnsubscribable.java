package net.rivercrew.domotics.omremote;

public interface IUnsubscribable {
	public void unsubscribe();
}
