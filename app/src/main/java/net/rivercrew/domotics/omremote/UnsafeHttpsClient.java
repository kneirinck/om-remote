package net.rivercrew.domotics.omremote;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;

public class UnsafeHttpsClient {
	public static OkHttpClient.Builder getUnsafeOkHttpClientBuilder() {
		try {
			// Create a trust manager that does not validate certificate chains
			final TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					@Override
					public X509Certificate[] getAcceptedIssuers() {
						return new X509Certificate[0];
					}

					@Override
					public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
					}

					@Override
					public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
					}
				}};

			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

			return new OkHttpClient.Builder()
				.sslSocketFactory(sslSocketFactory)
				.hostnameVerifier((hostname, session) -> true);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
