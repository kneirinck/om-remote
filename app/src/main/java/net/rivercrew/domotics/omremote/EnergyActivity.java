package net.rivercrew.domotics.omremote;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import net.rivercrew.domotics.omremote.utils.AutoRefreshTimer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EnergyActivity extends AppCompatActivity implements Foreground.Listener {
	@BindView(R.id.chart)
	HorizontalBarChart mChart;
	@BindView(R.id.spinner)
	Spinner mSpinner;
	private AutoRefreshTimer mAutoRefreshTimer;
	private Subscription mSubscription;
	private HashMap<String, Integer> mColors;
	private HashMap<Float, String> mLabels;
	private ChartDataType mChartDataType;
	private Map<String, OpenMoticsApi.TagData> mLastData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Foreground.get().addListener(this);

		this.setContentView(R.layout.activity_energy);
		ButterKnife.bind(this);

		mChart.getLegend().setEnabled(false);
		mChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
		mChart.getXAxis().setValueFormatter((value, axis) -> mLabels.containsKey(value) ? mLabels.get(value) : "");
		mChart.getXAxis().setDrawGridLines(false);
		mChart.getAxisRight().setDrawGridLines(false);
		mChart.getAxisRight().setAxisMinimum(0f);
		mChart.getAxisRight().setValueFormatter((value, axis) -> String.format(Locale.getDefault(), "%.0f", value));
		mChart.getAxisLeft().setDrawGridLines(false);
		mChart.getAxisLeft().setAxisMinimum(0f);
		mChart.getAxisLeft().setValueFormatter((value, axis) -> String.format(Locale.getDefault(), "%.0f", value));
		mChart.setDoubleTapToZoomEnabled(false);
		mChart.setPinchZoom(false);
		mChart.setTouchEnabled(false);

		mSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, ChartDataType.values()));
		mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mChartDataType = (ChartDataType) parent.getAdapter().getItem(position);
				if (mLastData != null) {
					updateChart(mLastData);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		mColors = new HashMap<>();
		mLabels = new HashMap<>();
		mChartDataType = ChartDataType.POWER;

		this.mSubscription = OpenMoticsClient.getInstance()
			.subscribeFor(OpenMoticsApi.MultipleMessages.class)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(multipleMessages -> {
				mLastData = multipleMessages.messages.get(multipleMessages.messages.size() - 1).data.tagsData;
				updateChart(mLastData);
			});

		this.mAutoRefreshTimer = new AutoRefreshTimer(this, AutoRefreshTimer.RefreshType.ENERGY);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onDestroy() {
		Foreground.get().removeListener(this);
		this.mAutoRefreshTimer.cancel();
		this.mSubscription.unsubscribe();
		super.onDestroy();
	}

	@Override
	public void onBecameForeground() {
		OpenMoticsClient.getInstance().checkConnectivity(true);
		if (!this.mAutoRefreshTimer.isRunning()) {
			this.mAutoRefreshTimer.resume();
		}
	}

	@Override
	public void onBecameBackground() {
		this.mAutoRefreshTimer.pause();
	}

	private void updateChart(Map<String, OpenMoticsApi.TagData> tagsData) {
		final BarData barData = new BarData();
		final Random rnd = new Random();
		float x = 0f;
		mLabels.clear();
		for (Map.Entry<String, OpenMoticsApi.TagData> entry : tagsData.entrySet()) {
			final float value;
			switch (mChartDataType) {
				default:
				case POWER:
					value = (float) entry.getValue().power;
					break;
				case COST:
					value = (float) entry.getValue().costs;
					break;
			}

			final BarDataSet barDataSet = new BarDataSet(
				Collections.singletonList(new BarEntry(x, Math.max(0f, value))),
				entry.getKey()
			);

			if (!mColors.containsKey(entry.getKey())) {
				mColors.put(entry.getKey(), Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
			}
			barDataSet.setColor(mColors.get(entry.getKey()));
			barDataSet.setHighlightEnabled(false);
			barDataSet.setValueFormatter((value1, entry1, dataSetIndex, viewPortHandler) -> String.format(Locale.getDefault(), "%.2f", value1));
			barDataSet.setValueTextSize(10);
			barDataSet.setBarBorderWidth(.5f);

			mLabels.put(x, entry.getKey());

			barData.addDataSet(barDataSet);
			x += 1;
		}
		mChart.setData(barData);
		mChart.getXAxis().setLabelCount(barData.getDataSetCount());
		final Description description = new Description();
		switch (mChartDataType) {
			default:
			case POWER:
				description.setText(getText(R.string.graph_description_power).toString());
				break;
			case COST:
				description.setText(getText(R.string.graph_description_cost).toString());
				break;
		}
		mChart.setDescription(description);

		mChart.invalidate();
	}

	enum ChartDataType {
		POWER("Power"),
		COST("Cost");

		private final String name;

		ChartDataType(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}
}
