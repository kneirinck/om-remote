package net.rivercrew.domotics.omremote;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

import net.rivercrew.domotics.omremote.widgets.WidgetHelper;

public class SettingsActivity extends PreferenceActivity {
	public final static String PREF_LOCAL_USERNAME = "pref_key_local_username";
	public final static String PREF_LOCAL_PASSWORD = "pref_key_local_password";
	public final static String PREF_LOCAL_IP = "pref_key_local_ip";
	public final static String PREF_LOCAL_TOKEN = "pref_key_local_token";
	public final static String PREF_REMOTE_USERNAME = "pref_key_remote_username";
	public final static String PREF_REMOTE_PASSWORD = "pref_key_remote_password";
	public final static String PREF_REMOTE_TOKEN = "pref_key_remote_token";
	public final static String PREF_REMOTE_INSTALLATION_ID = "pref_key_remote_installation_id";
	public final static String PREF_APP_AUTO_REFRESH_INTERVAL = "pref_key_app_auto_refresh_interval";
	public final static String PREF_APP_WIDGET_REFRESH_INTERVAL = "pref_key_app_widget_refresh_interval";
	public final static String PREF_APP_DEBUG = "pref_key_app_debug";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getFragmentManager()
			.beginTransaction()
			.replace(android.R.id.content, new SettingsFragment())
			.commit();

	}

	public static class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			this.addPreferencesFromResource(R.xml.preferences);
		}

		@Override
		public void onResume() {
			super.onResume();

			this.getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
		}

		@Override
		public void onPause() {
			super.onPause();

			this.getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
			OpenMoticsClient.getInstance().checkConnectivity();
		}

		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
			if (key.equals(PREF_LOCAL_USERNAME) || key.equals(PREF_LOCAL_PASSWORD) || key.equals(PREF_LOCAL_IP)) {
				// Invalidate local token
				sharedPreferences.edit().remove(PREF_LOCAL_TOKEN).apply();
				if (key.equals(PREF_LOCAL_IP)) {
					// Let the client know that the ip address changed
					OpenMoticsClient.getInstance().updateLocalIpAddress(getActivity());
				}
			} else if (key.equals(PREF_REMOTE_USERNAME) || key.equals(PREF_REMOTE_PASSWORD)) {
				// Invalidate remote token and installation id
				sharedPreferences.edit().remove(PREF_REMOTE_TOKEN).remove(PREF_REMOTE_INSTALLATION_ID).apply();
			} else if (key.equals(PREF_APP_DEBUG)) {
				OpenMoticsClient.getInstance().setDebug(sharedPreferences.getBoolean(PREF_APP_DEBUG, false));
			} else if (key.equals(PREF_APP_WIDGET_REFRESH_INTERVAL)) {
				WidgetHelper.updateAlarm(getActivity());
			}
		}
	}
}
