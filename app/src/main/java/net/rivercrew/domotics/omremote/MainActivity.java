package net.rivercrew.domotics.omremote;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import net.rivercrew.domotics.omremote.adapters.DrawerAdapter;
import net.rivercrew.domotics.omremote.adapters.FloorAdapter;
import net.rivercrew.domotics.omremote.utils.AutoRefreshTimer;
import net.rivercrew.domotics.omremote.views.SwipeDisableableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Foreground.Listener {
	@BindView(R.id.pager)
	protected SwipeDisableableViewPager mPager;
	@BindView(R.id.tabs)
	protected TabLayout mTabs;
	@BindView(R.id.toolbar)
	protected Toolbar mToolbar;
	@BindView(R.id.drawer_layout)
	protected DrawerLayout mDrawerLayout;
	@BindView(R.id.drawer)
	protected ListView mDrawer;
	private ActionBarDrawerToggle mDrawerToggle;
	private AutoRefreshTimer mAutoRefreshTimer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Foreground.get().addListener(this);

		this.setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		this.setSupportActionBar(this.mToolbar);

		final FloorAdapter floorAdapter = new FloorAdapter(this.getSupportFragmentManager());
		this.mPager.setAdapter(floorAdapter);

		// Setup tab layout to detect changes in the viewpager.
		this.mTabs.setupWithViewPager(this.mPager);

		this.mDrawerToggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, this.mToolbar, R.string.drawer_open, R.string.drawer_close);
		final DrawerAdapter drawerAdapter = new DrawerAdapter(this);
		this.mDrawer.setAdapter(drawerAdapter);
		this.mDrawer.setOnItemClickListener(drawerAdapter);

		this.mAutoRefreshTimer = new AutoRefreshTimer(this);
	}

	@Override
	protected void onDestroy() {
		Foreground.get().removeListener(this);
		((FloorAdapter) this.mPager.getAdapter()).unsubscribe();
		this.mAutoRefreshTimer.cancel();
		super.onDestroy();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		this.mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		this.mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (this.mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
			case R.id.refresh:
				OpenMoticsClient.getInstance().refresh();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getMenuInflater().inflate(R.menu.menu_main, menu);

		return super.onCreateOptionsMenu(menu);
	}

	private void setNetworkListener(boolean enabled) {
		final Context context = this.getApplicationContext();
		final ComponentName receiver = new ComponentName(context, NetworkStateReceiver.class);
		final PackageManager pm = context.getPackageManager();

		pm.setComponentEnabledSetting(receiver,
			(enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED),
			PackageManager.DONT_KILL_APP);
	}

	@Override
	public void onBecameForeground() {
		this.setNetworkListener(true);
		OpenMoticsClient.getInstance().checkConnectivity(true);
		if (!this.mAutoRefreshTimer.isRunning()) {
			this.mAutoRefreshTimer.resume();
		}
	}

	@Override
	public void onBecameBackground() {
		this.setNetworkListener(false);
		this.mAutoRefreshTimer.pause();
	}

	@Override
	public void onSupportActionModeStarted(@NonNull ActionMode mode) {
		super.onSupportActionModeStarted(mode);

		this.mTabs.setVisibility(View.INVISIBLE);
		this.mPager.setSwipeEnabled(false);
		if (this.mAutoRefreshTimer.isRunning()) {
			this.mAutoRefreshTimer.pause();
		}
	}

	@Override
	public void onSupportActionModeFinished(@NonNull ActionMode mode) {
		super.onSupportActionModeFinished(mode);

		this.mPager.setSwipeEnabled(true);
		this.mTabs.setVisibility(View.VISIBLE);
		if (!this.mAutoRefreshTimer.isRunning()) {
			this.mAutoRefreshTimer.resume();
		}
	}
}
