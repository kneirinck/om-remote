package net.rivercrew.domotics.omremote;

import android.content.SharedPreferences;
import android.text.TextUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;

public class TokenProviderInterceptor implements Interceptor, SharedPreferences.OnSharedPreferenceChangeListener {
	public static final int CODE_LOGIN_FAILED = 666;

	private final boolean mIsLocal;
	private final SharedPreferences mPreferences;
	private OpenMoticsApi.Token mToken = new OpenMoticsApi.Token();
	private OpenMoticsApi mApi;
	private int mInstallationId = -1;

	public TokenProviderInterceptor(SharedPreferences preferences, boolean isLocal) {
		this.mIsLocal = isLocal;
		this.mPreferences = preferences;

		// Call onSharedPreferenceChanged to unify the code to read the values.
		this.onSharedPreferenceChanged(preferences, SettingsActivity.PREF_LOCAL_TOKEN);
		this.onSharedPreferenceChanged(preferences, SettingsActivity.PREF_REMOTE_TOKEN);
		this.onSharedPreferenceChanged(preferences, SettingsActivity.PREF_REMOTE_INSTALLATION_ID);

		this.mPreferences.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void finalize() throws Throwable {
		this.mPreferences.unregisterOnSharedPreferenceChangeListener(this);

		super.finalize();
	}

	private Request addToken(Request origRequest) throws IOException {
		if (this.mToken == null || TextUtils.isEmpty(this.mToken.token)) {
			return null;
		}

		// Add token param to the body.
		final Buffer content = new Buffer();
		final RequestBody origBody = origRequest.body();
		origBody.writeTo(content);
		if (content.size() > 0) {
			content.writeByte('&');
		}
		content.writeString("token=", Charset.defaultCharset());
		content.writeString(URLEncoder.encode(this.mToken.token, "utf-8"), Charset.defaultCharset());

		final RequestBody newBody = RequestBody.create(
			MediaType.parse("application/x-www-form-urlencoded"), content.snapshot()
		);

		return origRequest.newBuilder()
			.method(origRequest.method(), newBody)
			.build();
	}

	private Request addInstallationId(Request origRequest) throws IOException {
		if (this.mIsLocal) {
			return origRequest;
		}

		if (origRequest.headers().names().contains(OpenMoticsApi.ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM)) {
			return origRequest.newBuilder()
				.removeHeader(OpenMoticsApi.ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM)
				.url(origRequest.url().newBuilder()
					.addQueryParameter("id", Integer.toString(this.mInstallationId))
					.build())
				.method(origRequest.method(), origRequest.body())
				.build();
		}

		// Add installation_id param to the body.
		final Buffer content = new Buffer();
		final RequestBody origBody = origRequest.body();
		origBody.writeTo(content);
		if (content.size() > 0) {
			content.writeByte('&');
		}
		content.writeString("installation_id=", Charset.defaultCharset());
		content.writeString(Integer.toString(this.mInstallationId), Charset.defaultCharset());

		final RequestBody newBody = RequestBody.create(
			MediaType.parse("application/x-www-form-urlencoded"), content.snapshot()
		);

		return origRequest.newBuilder()
			.method(origRequest.method(), newBody)
			.build();
	}

	private void updateToken() throws IOException {
		final String prefUsername;
		final String prefPassword;
		final String prefToken;
		if (this.mIsLocal) {
			prefUsername = SettingsActivity.PREF_LOCAL_USERNAME;
			prefPassword = SettingsActivity.PREF_LOCAL_PASSWORD;
			prefToken = SettingsActivity.PREF_LOCAL_TOKEN;
		} else {
			prefUsername = SettingsActivity.PREF_REMOTE_USERNAME;
			prefPassword = SettingsActivity.PREF_REMOTE_PASSWORD;
			prefToken = SettingsActivity.PREF_REMOTE_TOKEN;
		}

		final String username = this.mPreferences.getString(prefUsername, "");
		final String password = this.mPreferences.getString(prefPassword, "");

		if (TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
			// Nothing filled in yet, no need to continue, throw a generic exception.
			throw new IOException();
		}
		this.mToken = this.mApi.login(
			this.mPreferences.getString(prefUsername, ""),
			this.mPreferences.getString(prefPassword, "")
		).execute().body();
		if (this.mToken != null) {
			this.mPreferences.edit()
				.putString(prefToken, this.mToken.token)
				.apply();
		}
	}

	private void updateInstallationId() throws IOException {
		// Get the installation id.
		try {
			final List<OpenMoticsApi.Installation> installations = this.mApi.getInstallations().execute().body();
			if (installations != null && !installations.isEmpty()) {
				this.mInstallationId = installations.get(0).id;
				this.mPreferences.edit()
					.putInt(SettingsActivity.PREF_REMOTE_INSTALLATION_ID, this.mInstallationId)
					.apply();
			}
		} catch (IOException ignore) {
		}
	}

	/// Token expired or faulty, try logging in and retry.
	private Response requestToken(Response response, Chain chain) throws IOException {
		if (response != null) {
			response.body().close();
		}
		this.updateToken();
		final Request requestWithToken = this.addToken(chain.request());
		if (requestWithToken == null) {
			// Login failed
			return new Response.Builder()
				.request(chain.request())
				.protocol(Protocol.HTTP_1_1)
				.code(CODE_LOGIN_FAILED)
				.body(ResponseBody.create(MediaType.parse(""), ""))
				.build();
		}
		return chain.proceed(requestWithToken);
	}

	public void setApi(OpenMoticsApi api) {
		this.mApi = api;
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		final Request request = chain.request();
		Response response;

		final List<String> pathSegments = request.url().pathSegments();
		if (pathSegments.isEmpty() || !pathSegments.get(pathSegments.size() - 1).equals("login")) {
			Request adjustedRequest = request;
			if (!this.mIsLocal && !pathSegments.get(pathSegments.size() - 1).equals("get_installations")) {
				// For non-login calls that are remote add the installation_id parameter to the body.
				if (this.mInstallationId == -1) {
					this.updateInstallationId();
				}
				adjustedRequest = this.addInstallationId(adjustedRequest);
			}
			// For non-login calls add the token parameter to the body.
			adjustedRequest = this.addToken(adjustedRequest);
			if (adjustedRequest == null) {
				// No token yet, request one.
				response = requestToken(null, chain);
			} else {
				response = chain.proceed(adjustedRequest);
				if (response.code() == 401) {
					response = requestToken(response, chain);
				}
			}
		} else {
			response = chain.proceed(request);
		}
		return response;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (this.mIsLocal && key.equals(SettingsActivity.PREF_LOCAL_TOKEN)) {
			this.mToken.token = this.mPreferences.getString(SettingsActivity.PREF_LOCAL_TOKEN, "");
		} else if (!this.mIsLocal) {
			if (key.equals(SettingsActivity.PREF_REMOTE_TOKEN)) {
				this.mToken.token = this.mPreferences.getString(SettingsActivity.PREF_REMOTE_TOKEN, "");
			} else if (key.equals(SettingsActivity.PREF_REMOTE_INSTALLATION_ID)) {
				this.mInstallationId = this.mPreferences.getInt(SettingsActivity.PREF_REMOTE_INSTALLATION_ID, -1);
			}
		}
	}
}
