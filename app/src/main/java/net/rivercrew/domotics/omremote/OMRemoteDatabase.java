package net.rivercrew.domotics.omremote;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = OMRemoteDatabase.NAME, version = OMRemoteDatabase.VERSION)
public class OMRemoteDatabase {
	public final static String NAME = "OMRemote";
	public final static int VERSION = 5;
}
