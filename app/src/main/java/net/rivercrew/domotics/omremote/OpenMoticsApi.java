package net.rivercrew.domotics.omremote;

import android.annotation.SuppressLint;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface OpenMoticsApi {
	public static final String ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM = "addInstallationIdByGetQueryParam";
	public static final String ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM_HEADER = ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM + ": True";

	@FormUrlEncoded
	@POST("login")
	Call<Token> login(@Field("username") String username, @Field("password") String password);

	@POST("get_output_configurations")
	Observable<ConfigResult<List<OutputConfiguration>>> getOutputConfigurations();

	@POST("get_shutter_configurations")
	Observable<ConfigResult<List<ShutterConfiguration>>> getShutterConfigurations();

	@POST("get_output_status")
	Observable<StatusResult<List<OutputStatus>>> getOutputStatus();

	@POST("get_shutter_status")
	Observable<StatusResult<List<ShutterStatus>>> getShutterStatus();

	@FormUrlEncoded
	@POST("set_output")
	Observable<Result> setOutput(@Field("id") int id, @Field("is_on") boolean status, @Field("dimmer") int dimmerPercentage);

	@FormUrlEncoded
	@POST("set_output")
	Observable<Result> setOutput(@Field("id") int id, @Field("is_on") boolean status);

	@POST("do_shutter_up")
	Observable<Result> shutterUp(@Query("id") int id);

	@POST("do_shutter_down")
	Observable<Result> shutterDown(@Query("id") int id);

	@POST("do_shutter_stop")
	Observable<Result> shutterStop(@Query("id") int id);

	@POST("get_installations")
	Call<List<Installation>> getInstallations();

	@POST("get_status")
	Call<Result> getStatus();

	@Headers(ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM_HEADER)
	@FormUrlEncoded
	@POST("update_message_subscription")
	Observable<String> updateMessageSubscription(@Query("subscriber") int subscriberId, @Query("timestamp") int timestamp, @Field("types") String messageTypes);

	@Headers(ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM_HEADER)
	@POST("get_last_message_id")
	Observable<Integer> getLastMessageId(@Query("subscriber") int subscriberId, @Query("timestamp") int timestamp);

	@Headers(ADD_INSTALLATION_ID_BY_GET_QUERY_PARAM_HEADER)
	@POST("get_messages_wait")
	Observable<MultipleMessages> getMessages(@Query("subscriber") int subscriberId, @Query("timestamp") int timestamp, @Query("messageid") int messageId);

	class Token {
		public String token = "";
	}

	class Result {
		public boolean success;
	}

	class ConfigResult<ResultType> extends Result {
		public ResultType config;
	}

	class StatusResult<ResultType> extends Result {
		public ResultType status;
	}

	enum ModuleType {
		@SerializedName("O")
		OUTPUT,
		@SerializedName("R")
		ROLLER,
		@SerializedName("D")
		DIMMER,
		@SerializedName("I")
		INPUT,
		@SerializedName("T")
		TEMPERATURE,
		@SerializedName("L")
		OLED
	}

	@Table(database = OMRemoteDatabase.class)
	class OutputConfiguration extends BaseModel {
		@PrimaryKey
		public int id;
		@Column
		public byte floor;
		@Column
		public String name;
		@Column
		@SerializedName("module_type")
		public ModuleType moduleType;
		@Column
		public short timer;
		@Column
		public byte type;
	}

	@Table(database = OMRemoteDatabase.class)
	class ShutterConfiguration extends BaseModel {
		@PrimaryKey
		public int id;
		@Column
		public byte room;
		@Column
		public String name;
	}

	class OutputStatus {
		public int id;
		public int status;
		public int dimmer;
	}

	enum ShutterStatus {
		@SerializedName("going_up")
		GOING_UP,
		@SerializedName("going_down")
		GOING_DOWN,
		@SerializedName("stopped")
		STOPPED,
		@SerializedName("up")
		UP,
		@SerializedName("down")
		DOWN,
		UNKNOWN;

		public net.rivercrew.domotics.omremote.outputs.OutputStatus upStatus() {
			switch (this) {
				case GOING_UP:
				case UP:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.ON;
				case GOING_DOWN:
				case DOWN:
				case STOPPED:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.OFF;
				default:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.UNKNOWN;
			}
		}

		public net.rivercrew.domotics.omremote.outputs.OutputStatus downStatus() {
			switch (this) {
				case GOING_DOWN:
				case DOWN:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.ON;
				case GOING_UP:
				case UP:
				case STOPPED:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.OFF;
				default:
					return net.rivercrew.domotics.omremote.outputs.OutputStatus.UNKNOWN;
			}
		}
	}

	class Installation {
		public int id;
	}

	enum MessageType {
		@SerializedName("TYPE_OUTPUT_CHANGE")
		MSG_OUTPUT_CHANGE,
		@SerializedName("TYPE_THERMOSTAT_CHANGE")
		MSG_THERMOSTAT_CHANGE,
		@SerializedName("TYPE_RT_POWER_RAW")
		MSG_RT_POWER_RAW,
		@SerializedName("TYPE_RT_POWER_TAGS")
		MSG_RT_POWER_TAGS,
		@SerializedName("TYPE_RT_PULSE_COUNTERS")
		MSG_RT_PULSE_COUNTERS,
		@SerializedName("TYPE_RT_ENERGY_SUPPLIERS")
		MSG_RT_ENERGY_SUPPLIERS,
		@SerializedName("TYPE_RT_ENERGY_MODULES")
		MSG_RT_ENERGY_MODULES,
		@SerializedName("TYPE_RT_ENERGY_TAGS")
		MSG_RT_ENERGY_TAGS
	}

	class MultipleMessages {
		@SerializedName("last_message_id")
		int lastMessageId;
		@SerializedName("subscribed_types")
		List<MessageType> subscribedTypes;
		List<Message> messages;
	}

	class Message {
		double timestamp;
		int stream;
		int id;
		MessageType type;
		MessageData data;
	}

	class MessageData {
		MessageDataMetadata metadata;
		@SerializedName("data")
		Map<String, TagData> tagsData;
	}

	class MessageDataMetadata {
		double timestamp;
	}

	class TagData {
		double power;
		@SerializedName("powerfact")
		double powerFactor;
		@SerializedName("apppower")
		double apparentPower;
		double costs;
		double current;
		double frequency;
		double voltage;

		@SuppressLint("DefaultLocale")
		@Override
		public String toString() {
			return String.format("{power:%.2f, powerfactor:%.2f, apparentpower: %.2f, costs: %.2f, current: %.2f, frequency: %.2f, voltage: %.2f",
				power, powerFactor, apparentPower, costs, current, frequency, voltage);
		}
	}
}
