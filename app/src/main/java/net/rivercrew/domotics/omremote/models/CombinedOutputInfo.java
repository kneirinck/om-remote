package net.rivercrew.domotics.omremote.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import net.rivercrew.domotics.omremote.OMRemoteDatabase;

@Table(database = OMRemoteDatabase.class)
public class CombinedOutputInfo extends BaseModel {
	@PrimaryKey(autoincrement = true)
	public int id;
	@Column
	public int combinedId;
	@Column
	public int outputId;
	@Column
	public String name;

	public CombinedOutputInfo() {
		super();
	}

	public CombinedOutputInfo(int combinedId, String name, int outputId) {
		super();

		this.combinedId = combinedId;
		this.name = name;
		this.outputId = outputId;
	}
}
